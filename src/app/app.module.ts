import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER , LOCALE_ID} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminComponent } from './layout/admin/admin.component';
import { AuthComponent } from './layout/auth/auth.component';
import { NavigationComponent } from './layout/admin/navigation/navigation.component';
import { NavContentComponent } from './layout/admin/navigation/nav-content/nav-content.component';
import { NavGroupComponent } from './layout/admin/navigation/nav-content/nav-group/nav-group.component';
import { NavCollapseComponent } from './layout/admin/navigation/nav-content/nav-collapse/nav-collapse.component';
import { NavItemComponent } from './layout/admin/navigation/nav-content/nav-item/nav-item.component';
import { NavBarComponent } from './layout/admin/nav-bar/nav-bar.component';
import { NavLeftComponent } from './layout/admin/nav-bar/nav-left/nav-left.component';
import { NavSearchComponent } from './layout/admin/nav-bar/nav-left/nav-search/nav-search.component';
import { NavRightComponent } from './layout/admin/nav-bar/nav-right/nav-right.component';
import { ConfigurationComponent } from './layout/admin/configuration/configuration.component';
import { ToggleFullScreenDirective } from './modules/shared/full-screen/toggle-full-screen';
import { SharedModule } from './modules/shared/shared.module';
import { NavigationItem } from './layout/admin/navigation/navigation';
import { NgbModule, NgbDropdownModule, NgbTooltipModule, NgbButtonsModule, NgbTabsetModule, NgbDateAdapter, NgbDateParserFormatter, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule, registerLocaleData } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { JwtInterceptor } from './modules/shared/interceptors/jwt.interceptor';
import { ErrorInterceptor } from './modules/shared/interceptors/error.interceptor';
import { CustomAdapter, CustomDateParserFormatter, I18n, CustomDatepickerI18n } from './modules/shared/utils/datepicker.adapter';
import {NgxWebstorageModule} from 'ngx-webstorage';
import { LoadEnumsService } from './modules/shared/services/load-enums.service';
import { ToastrModule } from 'ngx-toastr';
import { NgxMaskModule } from 'ngx-mask';
import localePt from '@angular/common/locales/pt';
import { NgxCurrencyModule } from "ngx-currency";
import { NgxUiLoaderModule } from 'ngx-ui-loader';

registerLocaleData(localePt);


@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    AuthComponent,
    NavigationComponent,
    NavContentComponent,
    NavGroupComponent,
    NavCollapseComponent,
    NavItemComponent,
    NavBarComponent,
    NavLeftComponent,
    NavSearchComponent,
    NavRightComponent,
    ConfigurationComponent,
    ToggleFullScreenDirective,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    NgbModule,
    NgxWebstorageModule.forRoot(),
    ToastrModule.forRoot(),
    NgxMaskModule.forRoot(),
    NgxUiLoaderModule

  ],
  providers: [
    NavigationItem,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    {provide: NgbDateAdapter, useClass: CustomAdapter},
    {provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter},
    [I18n, { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n }],
    { provide: LOCALE_ID, useValue: 'pt'},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
