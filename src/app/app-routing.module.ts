import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './layout/admin/admin.component';
import { AuthComponent } from './layout/auth/auth.component';
import { AppGuard } from './app.guard';

const routes: Routes = [
  {
        path: '',
        redirectTo: '/auth/login',
        pathMatch: 'full'
  },
  {
    path: 'pages',
    component: AdminComponent,
    children: [
      {
        path: '',
        redirectTo: '/dashboard/',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./modules/dashboard/dashboard.module').then(module => module.DashboardModule),
        canActivate: [AppGuard]
      },
      {
        path: 'usuarios',
        loadChildren: () => import('./modules/usuarios/usuarios.module').then(module => module.UsuarioModule),
        canActivate: [AppGuard]
      },
      {
        path: 'servidores',
        loadChildren: () => import('./modules/servidores/servidores.module').then(module => module.ServidoresModule),
        canActivate: [AppGuard]
      },
      {
        path: 'faqs',
        loadChildren: () => import('./modules/faqs/faqs.module').then(module => module.FaqsModule),
        canActivate: [AppGuard]
      },
      {
        path: 'avisos',
        loadChildren: () => import('./modules/avisos/avisos.module').then(module => module.AvisosModule),
        canActivate: [AppGuard]
      },

      {
        path: 'consignataria',
        loadChildren: () => import('./modules/consignatarias/consignatarias.module').then(module => module.ConsignatariasModule),
        canActivate: [AppGuard]
      },
      {
        path: 'produtos',
        loadChildren: () => import('./modules/produtos/produtos.module').then(module => module.ProdutosModule),
        canActivate: [AppGuard]
      },
      {
        path: 'relatorios',
        loadChildren: () => import('./modules/relatorios/relatorios.module').then(module => module.RelatoriosModule),
        canActivate: [AppGuard]
      },
      {
        path: 'propostas',
        loadChildren: () => import('./modules/propostas/propostas.module').then(module => module.PropostasModule),
        canActivate: [AppGuard]
      },
      {
        path: 'emprestimos',
        loadChildren: () => import('./modules/consignacoes/consignacoes.module').then(module => module.ConsignacoesModule),
        canActivate: [AppGuard]
      },
      {
        path: 'cartoes',
        loadChildren: () => import('./modules/cartoes/cartoes.module').then(module => module.CartoesModule),
        canActivate: [AppGuard]
      },
      {
        path: 'portabilidade',
        loadChildren: () => import('./modules/portabilidade/portabilidade.module').then(module => module.PortabilidadeModule),
        canActivate: [AppGuard]
      },
      {
        path: 'renegociacao',
        loadChildren: () => import('./modules/renegociacao/renegociacao.module').then(module => module.RenegociacaoModule),
        canActivate: [AppGuard]
      },
    ]
  },
  {
    path: 'auth',
    component: AuthComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./modules/auth/authentication.module').then(module => module.AuthenticationModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
