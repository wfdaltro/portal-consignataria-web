import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { NgxMaskModule } from 'ngx-mask';
import { DataTablesModule } from 'angular-datatables';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { ConsultaConsignacoesComponent } from './pages/consulta-consignacoes/consulta-consignacoes.component';
import { DetalhaConsignacaoComponent } from './pages/detalha-consignacao/detalha-consignacao.component';
import { DetalhaParcelasComponent } from './pages/detalha-parcelas/detalha-parcelas.component';
import { SuspenderEmprestimoComponent } from './pages/suspender-emprestimo/suspender-emprestimo.component';
import { LiquidarEmprestimoComponent } from './pages/liquidar-emprestimo/liquidar-emprestimo.component';
import { LiquidarParcelaComponent } from './pages/liquidar-parcela/liquidar-parcela.component';
import { GerenciarAnexosComponent } from './pages/gerenciar-anexos/gerenciar-anexos.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { AtivarEmprestimoComponent } from './pages/ativar/ativar-emprestimo.component';

const routes: Routes = [
  {path: '', component: ConsultaConsignacoesComponent,   pathMatch: 'full'},
  {path: ':id', component: DetalhaConsignacaoComponent,   pathMatch: 'full'},
  {path: ':id/parcelas', component: DetalhaParcelasComponent,   pathMatch: 'full'},
  {path: ':id/suspender', component: SuspenderEmprestimoComponent,   pathMatch: 'full'},
  {path: ':id/ativar', component: AtivarEmprestimoComponent,   pathMatch: 'full'},
  {path: ':id/anexos', component: GerenciarAnexosComponent,   pathMatch: 'full'},
  {path: ':id/liquidar', component: LiquidarEmprestimoComponent,   pathMatch: 'full'},
  {path: ':id/parcelas/:idParcela/liquidar', component: LiquidarParcelaComponent,   pathMatch: 'full'}
];


@NgModule({
  declarations: [ConsultaConsignacoesComponent, DetalhaConsignacaoComponent, DetalhaParcelasComponent,
    SuspenderEmprestimoComponent, LiquidarEmprestimoComponent,
    LiquidarParcelaComponent, GerenciarAnexosComponent, AtivarEmprestimoComponent],
  imports: [
  RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgxMaskModule.forRoot(),
    DataTablesModule,
    NgbModule,
    NgSelectModule,
     NgxDropzoneModule,
  ]
})
export class ConsignacoesModule { }
