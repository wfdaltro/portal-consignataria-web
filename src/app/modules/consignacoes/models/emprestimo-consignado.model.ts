import { Servidor } from 'src/app/modules/servidores/models/servidor.model';
import { Consignataria } from 'src/app/modules/consignatarias/models/consignataria.model';
import { Enum } from 'src/app/modules/shared/models/enum.model';

export class EmprestimoConsignado   {


  pk: number;

servidor: Servidor;


	 consignataria: Consignataria;


	situacao: Enum;


	 valorTotalFinanciamento: number;


valorFinanciado: number;


 valorPrestacao: number;


totalSaldoResidual: number;


	totalDeParcelas: number;


	parcelasEmAberto: number;


jurosAoMes: number;


	 tac: number;


 iof: number;


	  ade: string;

}
