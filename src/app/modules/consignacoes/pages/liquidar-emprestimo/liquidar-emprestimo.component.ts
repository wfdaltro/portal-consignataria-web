import { Component, OnInit } from '@angular/core';
import { EmprestimoConsignado } from '../../models/emprestimo-consignado.model';
import { ConsignacoesService } from '../../services/consignacoes.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-liquidar-emprestimo',
  templateUrl: './liquidar-emprestimo.component.html',
  styleUrls: ['./liquidar-emprestimo.component.scss']
})
export class LiquidarEmprestimoComponent implements OnInit {


  emprestimo: EmprestimoConsignado;
  observacoes: string;
  dataLiquidacao: string;

  constructor(private service: ConsignacoesService, private toastr: ToastrService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.service.findOne(+id).subscribe((res)=>{
      this.emprestimo = res;
    });
  }

  suspender(){
    if (!this.dataLiquidacao){
      this.toastr.error(`Informe a data de liquidação do contrato.`,'Erro ao processar a requisição', {
        timeOut: 3000
      });
      return;
    }
    this.service.gravarLiquidacaoEmprestimo(this.emprestimo.pk, '3', this.observacoes, this.dataLiquidacao).subscribe((res)=>{
      this.toastr.success('Empréstimo consignado suspenso com sucesso.', 'Sucesso!', {
        timeOut: 3000
      });
      this.router.navigate([`/pages/emprestimos`]);
    },(err)=>{
      this.toastr.error(err,'Erro ao processar a requisição', {
        timeOut: 3000
      });
    });
  }

  cancelar(){
    this.router.navigate([`/pages/emprestimos/${this.emprestimo.pk}`]);
  }


}
