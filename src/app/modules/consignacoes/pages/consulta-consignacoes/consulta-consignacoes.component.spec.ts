import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaConsignacoesComponent } from './consulta-consignacoes.component';

describe('ConsultaConsignacoesComponent', () => {
  let component: ConsultaConsignacoesComponent;
  let fixture: ComponentFixture<ConsultaConsignacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultaConsignacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaConsignacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
