import { Component, OnInit } from '@angular/core';
import { EmprestimoConsignado } from '../../models/emprestimo-consignado.model';
import { Subject } from 'rxjs';
import { DATATABLE_PT } from 'src/app/modules/shared/constantes/datatable';
import { Router } from '@angular/router';
import { ConsignacoesService } from '../../services/consignacoes.service';
import { Enum } from 'src/app/modules/shared/models/enum.model';
import { Servidor } from 'src/app/modules/servidores/models/servidor.model';
import { EnumsService } from 'src/app/modules/shared/services/enums.service';
import { ServidoresService } from 'src/app/modules/servidores/services/servidores.service';

@Component({
  selector: 'app-consulta-consignacoes',
  templateUrl: './consulta-consignacoes.component.html',
  styleUrls: ['./consulta-consignacoes.component.scss']
})
export class ConsultaConsignacoesComponent implements OnInit {

  emprestimos: EmprestimoConsignado[];
  filtroPesquisa: any = {};
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  situacoes: Enum[]
  servidores: Servidor[];

  constructor(private router: Router, private consignacoesService: ConsignacoesService,
    private enumsService: EnumsService, private servidoresService: ServidoresService){ }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language: DATATABLE_PT
    };
    this.carregarDados();
    this.carregarServidores();
    this.carregarSituacoes();
  }

  carregarServidores() {
   this.servidoresService.findByParams().subscribe((res)=>{
     this.servidores = res
   });
  }

  carregarSituacoes() {
    this.situacoes =  this.enumsService.situacaoConsignacao;
  }

 carregarDados() {
    this.consignacoesService.findByParams(this.filtroPesquisa).subscribe((res)=>{
      this.emprestimos = res;
      console.log(this.emprestimos);
      this.dtTrigger.next();
    },(error)=>{
      console.log(error);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  public pesquisar(): void {
    this.consignacoesService.findByParams(this.filtroPesquisa).subscribe((res)=>{
      this.emprestimos = res;
    },(error)=>{
      console.log(error);
    });
  }

   detalhar(emprestimo: EmprestimoConsignado){
    this.router.navigate([`/pages/emprestimos/${emprestimo.pk}`]);
  }

  parcelas(emprestimo: EmprestimoConsignado){
    this.router.navigate([`/pages/emprestimos/${emprestimo.pk}/parcelas`]);
  }

  suspender(emprestimo: EmprestimoConsignado){
    this.router.navigate([`/pages/emprestimos/${emprestimo.pk}/suspender`]);
  }

  ativar(emprestimo: EmprestimoConsignado){
    this.router.navigate([`/pages/emprestimos/${emprestimo.pk}/ativar`]);
  }

  liquidar(emprestimo: EmprestimoConsignado){
    this.router.navigate([`/pages/emprestimos/${emprestimo.pk}/liquidar`]);
  }


  anexos(emprestimo: EmprestimoConsignado){
    this.router.navigate([`/pages/emprestimos/${emprestimo.pk}/anexos`]);
  }

}
