import { Component, OnInit } from '@angular/core';
import { EmprestimoConsignado } from '../../models/emprestimo-consignado.model';
import { ConsignacoesService } from '../../services/consignacoes.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detalha-consignacao',
  templateUrl: './detalha-consignacao.component.html',
  styleUrls: ['./detalha-consignacao.component.scss']
})
export class DetalhaConsignacaoComponent implements OnInit {

  emprestimo: EmprestimoConsignado;

  constructor(private service: ConsignacoesService, private toastr: ToastrService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.service.findOne(+id).subscribe((res)=>{
      this.emprestimo = res;
    });
  }


  parcelas(){
    this.router.navigate([`/pages/emprestimos/${this.emprestimo.pk}/parcelas`]);
  }

  suspender(){
    this.router.navigate([`/pages/emprestimos/${this.emprestimo.pk}/suspender`]);
  }
s
  ativar(){
    this.router.navigate([`/pages/emprestimos/${this.emprestimo.pk}/ativar`]);
  }

  liquidar(){
    this.router.navigate([`/pages/emprestimos/${this.emprestimo.pk}/liquidar`]);
  }

  anexos(){
    this.router.navigate([`/pages/emprestimos/${this.emprestimo.pk}/anexos`]);
  }

}
