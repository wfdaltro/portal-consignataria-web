import { Component, OnInit } from '@angular/core';
import { ParcelaEmprestimoConsignado } from '../../models/parcela-emprestimo.model';
import { Router, ActivatedRoute } from '@angular/router';
import { ConsignacoesService } from '../../services/consignacoes.service';

@Component({
  selector: 'app-detalha-parcelas',
  templateUrl: './detalha-parcelas.component.html',
  styleUrls: ['./detalha-parcelas.component.scss']
})
export class DetalhaParcelasComponent implements OnInit {

  parcelas: ParcelaEmprestimoConsignado[];
  id: number;

  constructor(private router: Router, private route: ActivatedRoute,  private consignacoesService: ConsignacoesService) { }

  ngOnInit(): void {

    this.carregarDados();
  }

 carregarDados() {
  this.id = +this.route.snapshot.paramMap.get('id');
    this.consignacoesService.findParcelasEmprestimo(this.id).subscribe((res)=>{
      this.parcelas = res;
      console.log(this.parcelas);
    },(error)=>{
      console.log(error);
    });
  }

}
