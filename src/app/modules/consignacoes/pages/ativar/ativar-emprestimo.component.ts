import { Component, OnInit } from '@angular/core';
import { EmprestimoConsignado } from '../../models/emprestimo-consignado.model';
import { ConsignacoesService } from '../../services/consignacoes.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ativar-emprestimo',
  templateUrl: './ativar-emprestimo.component.html',
  styleUrls: ['./ativar-emprestimo.component.scss']
})
export class AtivarEmprestimoComponent implements OnInit {

  emprestimo: EmprestimoConsignado;
  motivoAtivacao: string;                        

  constructor(private service: ConsignacoesService, private toastr: ToastrService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.service.findOne(+id).subscribe((res)=>{
      this.emprestimo = res;
    });
  }

  ativar(){
    if (!this.motivoAtivacao){
      this.toastr.error(`Informe o  motivo  da ativação do contrato.`,'Erro ao processar a requisição', {
        timeOut: 3000
      });
      return;
    }
    this.service.gravarOcorrenciaEmprestimo(this.emprestimo.pk, '1', this.motivoAtivacao).subscribe((res)=>{
      this.toastr.success('Empréstimo consignado ativado com sucesso.', 'Sucesso!', {
        timeOut: 3000
      });
      this.router.navigate([`/pages/emprestimos`]);
    },(err)=>{
      this.toastr.error(err,'Erro ao processar a requisição', {
        timeOut: 3000
      });
    });
  }



  cancelar(){
    this.router.navigate([`/pages/emprestimos/${this.emprestimo.pk}`]);
  }


}
