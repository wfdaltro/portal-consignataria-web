import { Component, OnInit } from '@angular/core';
import { EmprestimoConsignado } from '../../models/emprestimo-consignado.model';
import { ConsignacoesService } from '../../services/consignacoes.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-suspender-emprestimo',
  templateUrl: './suspender-emprestimo.component.html',
  styleUrls: ['./suspender-emprestimo.component.scss']
})
export class SuspenderEmprestimoComponent implements OnInit {


  emprestimo: EmprestimoConsignado;
  motivoSuspensao: string;

  constructor(private service: ConsignacoesService, private toastr: ToastrService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.service.findOne(+id).subscribe((res)=>{
      this.emprestimo = res;
    });
  }

  suspender(){
    if (!this.motivoSuspensao){
      this.toastr.error(`Informe o  motivo  da suspensão do contrato.`,'Erro ao processar a requisição', {
        timeOut: 3000
      });
      return;
    }
    this.service.gravarOcorrenciaEmprestimo(this.emprestimo.pk, '2', this.motivoSuspensao).subscribe((res)=>{
      this.toastr.success('Empréstimo consignado suspenso com sucesso.', 'Sucesso!', {
        timeOut: 3000
      });
      this.router.navigate([`/pages/emprestimos`]);
    },(err)=>{
      this.toastr.error(err,'Erro ao processar a requisição', {
        timeOut: 3000
      });
    });
  }



  cancelar(){
    this.router.navigate([`/pages/emprestimos/${this.emprestimo.pk}`]);
  }


}
