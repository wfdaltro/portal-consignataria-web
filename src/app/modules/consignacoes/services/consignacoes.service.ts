import { Injectable } from '@angular/core';
import { EmprestimoConsignado } from '../models/emprestimo-consignado.model';
import { HttpService } from '../../shared/services/http.service';
import { Observable } from 'rxjs';
import { ParcelaEmprestimoConsignado } from '../models/parcela-emprestimo.model';
import { HttpRequest } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConsignacoesService {



  private path: string = "emprestimosConsignados";

  constructor(private http: HttpService<any, number>) {
  }

  findOne(id: number): Observable<EmprestimoConsignado> {
    return this.http.getOne(this.path, id);
  }

  findAll(): Observable<EmprestimoConsignado[]> {
    return this.http.get(this.path);
  }

  findByParams(params?: any): Observable<EmprestimoConsignado[]> {
    return this.http.getParams(this.path + '/search', params);
  }

  findParcelasEmprestimo(id: number) : Observable<ParcelaEmprestimoConsignado[]>{
    return this.http.get(this.path +"/parcelas?id="+id) ;
  }

  gravarOcorrenciaEmprestimo(pk: number, acao: string, motivoOcorrencia: string) {
    let dadosOperacao: any = {
      observacao: motivoOcorrencia,
      acao: acao,
    }
    return this.http.pathResource(pk, `${this.path}/ocorrencia`, dadosOperacao);
  }

  gravarLiquidacaoEmprestimo(pk: number, acao: string, observacoes: string, dataLiquidacao: string) {
    let dadosOperacao: any = {
      observacao: observacoes,
      acao: acao,
      dataLiquidacao: dataLiquidacao
    }
    console.log(dadosOperacao);
    return this.http.pathResource(pk, `${this.path}/ocorrencia`, dadosOperacao);
  }

  carregarDocumentos(pk: number) {
    return this.http.get(this.path +"/anexos?id="+pk) ;
  }

  adicionarDocumentos(pk: number, nome: string, file: File){
    const formData = new FormData();
    formData.append("file", file);
    const req = new HttpRequest('POST', `${environment.apiUrl}/${this.path}/anexo/add?idEmprestimo=${pk}&nome=${nome}`, formData, {
      reportProgress: true,
      responseType: 'json'
    });
    return this.http.http.request(req);
  }

  download(pk: number) {
    return this.http.http.get(`${environment.apiUrl}/${this.path}/anexo/download?idAnexo=${pk}`, {responseType: 'blob'});
    }


}
