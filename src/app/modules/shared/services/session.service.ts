import { Injectable } from '@angular/core';
import { SessionStorageService } from 'ngx-webstorage';

@Injectable({
    providedIn: 'root'
})
export class SessionService{

    constructor(private session: SessionStorageService) {}

    setSessionToken(token: String){
        this.session.store('jwt', token);
    }

    getSessionToken(): String{
        return this.session.retrieve('jwt');
    }

    setUserInfo(userInfo: any){
        console.log('na session', userInfo);
        this.session.store('userInfo', userInfo);
    }

    getUserInfo(): any {
        return this.session.retrieve('userInfo');
    }

    clear(){
        this.session.clear();
    }
}
