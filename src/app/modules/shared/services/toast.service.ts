import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ToastService {

    toasts: any[] = [];

    showSuccess(body: string){
        this.show('Mensagem', body, 'success', 5000, 'fe-check');
    }

    showInfo(body: string){
        this.show('Aviso', body, 'info', 5000, 'mdi mdi-bell-alert');
    }

    showError(body: string){
        this.show('Erro', body, 'danger', 5000, 'mdi mdi-close-circle');
    }

    showWarning(body: string){
        this.show('Atenção', body, 'warning', 5000, 'fe-alert-triangle');
    }
s
    private show(header: string, body: string, className: string, delay: number, icon: string) {
        this.toasts.push({
            header: header,
            body: body,
            className: className,
            delay: delay,
            icon: icon
        });
    }

    remove(toast) {
        this.toasts = this.toasts.filter(t => t != toast);
    }

}
