import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ErrorHandler } from '../exceptions/error.handler';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService<T, ID>   {

   private apiUrl: string = environment.apiUrl;

  private  headers: HttpHeaders;

  public httpOptions: {
      headers: HttpHeaders,
  };

  constructor(public http: HttpClient) {
      this.httpOptions = { headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*'
      })};
  }

  public post(url: string,  model: FormData| T): Observable<T> {
      return this.http.post<T>(`${this.apiUrl}/${url}`, model, this.httpOptions).pipe(
          catchError(ErrorHandler.handleError)
      );
  }

  public get(url: string): Observable<T[]> {
      return this.http.get<T[]>( `${this.apiUrl}/${url}`, this.httpOptions).pipe(
          catchError(ErrorHandler.handleError)
      );
  }

  public getParams(url: string, params?: any): Observable<T[]> {
    console.log(params);
      if (params) {
          this.httpOptions['params'] = this.mountParams(params);
      }
      console.log(this.httpOptions)
      return this.http.get<T[]>( `${this.apiUrl}/${url}`, this.httpOptions).pipe(
          catchError(ErrorHandler.handleError)
      );
  }

  public getOne(url: string, id: ID): Observable<T> {
    return this.http.get<any>(`${this.apiUrl}/${url}/${id}`, this.httpOptions).pipe(
      catchError(ErrorHandler.handleError)
    );
  }

  public put(url: string, id: ID, model: T): Observable<T> {
      return this.http.put<T>(`${this.apiUrl}/${url}/${id}`, model, this.httpOptions).pipe(
          catchError(ErrorHandler.handleError)
      );
  }

  public patch(url: string, id: ID, model: FormData | T): Observable<T> {
      return this.http.patch<T>(`${this.apiUrl}/${url}/${id}`, model, this.httpOptions).pipe(
          catchError(ErrorHandler.handleError)
      );
  }

  public delete(url: string, id: ID): Observable<void> {
      return this.http.delete<void>(`${this.apiUrl}/${url}/${id}`, this.httpOptions).pipe(
          catchError(ErrorHandler.handleError)
      );
  }

  private mountParams(params?: any): {[param: string]: string} {
    console.log('pParametros:' , params);
      const param: {[param: string]: string} = {};
      if (params) {
          let map = new Map();
          Object.keys(params).forEach(key => {   
            console.log(key, params[key]);
              map.set(key, params[key]);
          });
          map.forEach((value: string, key: string) => {
              if (value != undefined &&  value.length > 0){
                  param[key] = value;
              }
          });
      }
      return param;
  }

  public getResource(resource:string): Observable<any>{
    return this.http.get<any>(`${this.apiUrl}/${resource}`, this.httpOptions).pipe(
      catchError(ErrorHandler.handleError)
    );
  }

  postResource(id: number,url: string, model: any): Observable<any> {
    return this.http.post<T>(`${this.apiUrl}/${url}/${id}`, model, this.httpOptions).pipe(
        catchError(ErrorHandler.handleError)
    );
  }

  pathResource(id: number, url: string, model?: any): Observable<any> {
    return this.http.patch<any>(`${this.apiUrl}/${url}/${id}`, model, this.httpOptions).pipe(
      catchError(ErrorHandler.handleError)
    );
  }

  upload(req: HttpRequest<FormData>): Observable<any> {
    return this.http.request(req);
  }

}
