import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Enum } from '../models/enum.model';
import { catchError } from 'rxjs/operators';
import { ErrorHandler } from '../exceptions/error.handler';
import { ENUMS } from '../constantes/enums';
import { EnumsService } from './enums.service';

@Injectable({
  providedIn: 'root'
})
export class LoadEnumsService   {

  protected httpOptions: {
    headers: HttpHeaders,
  };

  apiUrl: string;

  constructor(private http: HttpClient, private enumsService: EnumsService) {
    this.apiUrl = environment.apiUrl;
    this.httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' })};
   }


  carregarProdutoConsignado() : void {
    this.getEnum(ENUMS.produtoConsignado).subscribe(res=>{
      this.enumsService.produtoConsignado = res;
      console.log(this.enumsService.produtoConsignado);
    });
  }

  carregarSexos() : void {
    this.getEnum(ENUMS.sexo).subscribe(res=>{
      this.enumsService.sexo = res;
      console.log(this.enumsService.sexo);
    });
  }
  car
  carregarDestinatarioAviso() : void {
    this.getEnum(ENUMS.destinatarioAviso).subscribe(res=>{
      this.enumsService.destinatarioAviso = res;
      console.log(this.enumsService.destinatarioAviso);
    });
  }
  carregarEstados() : void {
    this.getEnum(ENUMS.estado).subscribe(res=>{
      this.enumsService.estado = res;
      console.log(this.enumsService.estado);
    });
  }
  carregarEstadosCivil() : void {
    this.getEnum(ENUMS.estadoCivil).subscribe(res=>{
      this.enumsService.estadoCivil = res;
      console.log(this.enumsService.estadoCivil);
    });
  }

  carregarSituacaoEntidadeConsignataria() : void {
    this.getEnum(ENUMS.situacaoEntidadeConsignataria).subscribe(res=>{
      this.enumsService.situacaoEntidadeConsignataria = res;
      console.log(this.enumsService.situacaoEntidadeConsignataria);
    });
  }

  carregarSituacaoUsuario() : void {
    this.getEnum(ENUMS.situacaoUsuario).subscribe(res=>{
      this.enumsService.situacaoUsuario = res;
      console.log(this.enumsService.situacaoUsuario);
    });
  }

  carregarMotivoBloqueio() : void {
    this.getEnum(ENUMS.motivoBloqueio).subscribe(res=>{
      this.enumsService.motivoBloqueio = res;
      console.log(this.enumsService.motivoBloqueio);
    });
  }

  carregarMotivoBloqueioServidor() : void {
    this.getEnum(ENUMS.motivoBloqueioServidor).subscribe(res=>{
      this.enumsService.motivoBloqueioServidor = res;
      console.log(this.enumsService.motivoBloqueioServidor);
    });
  }

  carregarTipoOperacaoSistema() : void {
    this.getEnum(ENUMS.tipoOperacaoSistema).subscribe(res=>{
      this.enumsService.tipoOperacaoSistema = res;
      console.log(this.enumsService.tipoOperacaoSistema);
    });
  }



  load(): Promise<any>{
    this.carregarSexos();
    this.carregarDestinatarioAviso();
    this.carregarEstados();
    this.carregarEstadosCivil();
    this.carregarSituacaoEntidadeConsignataria();
    this.carregarMotivoBloqueio();
    this.carregarMotivoBloqueioServidor();
    this.carregarTipoOperacaoSistema();
    this.carregarSituacaoUsuario();
    this.carregarProdutoConsignado();
    this.carregarSituacaoProposta();
    this.carregarSituacaoConsignacao();
    this.carregarSituacaoCartao();
    this.carregarSituacaoPortabilidade();
    this.carregarTipoRegistroPortabilidade();
    return Promise.resolve();
  }

  carregarSituacaoProposta() : void {
    this.getEnum(ENUMS.situacaoProposta).subscribe(res=>{
      this.enumsService.situacaoProposta = res;
      console.log('alerta vermelho:' , this.enumsService.situacaoProposta);
    });
  }

  carregarSituacaoConsignacao() : void {
    this.getEnum(ENUMS.situacaoConsignacao).subscribe(res=>{
      this.enumsService.situacaoConsignacao = res;
      console.log('alerta vermelho:' , this.enumsService.situacaoConsignacao);
    });
  }

  carregarSituacaoCartao() : void {
    this.getEnum(ENUMS.situacaoCartao).subscribe(res=>{
      this.enumsService.situacaoCartao = res;
      console.log('alerta vermelho:' , this.enumsService.situacaoCartao);
    });
  }

  carregarSituacaoPortabilidade() : void {
    this.getEnum(ENUMS.situacaoPortabilidade).subscribe(res=>{
      this.enumsService.situacaoPortabilidade = res;
      console.log('alerta vermelho:' , this.enumsService.situacaoPortabilidade);
    });
  }

  carregarTipoRegistroPortabilidade() : void {
    this.getEnum(ENUMS.tipoRegistroPortabilidade).subscribe(res=>{
      this.enumsService.tipoRegistroPortabilidade = res;
      console.log('alerta vermelho:' , this.enumsService.tipoRegistroPortabilidade);
    });
  }


  private getEnum(enumName:string){
    let url = `${environment.apiUrl}/enums?nome=${enumName}`;
    return this.getResource(url);
  }

  public getResource(resource:string): Observable<any>{
    return this.http.get<any>(resource, this.httpOptions).pipe(
      catchError(ErrorHandler.handleError)
    );
  }

}
