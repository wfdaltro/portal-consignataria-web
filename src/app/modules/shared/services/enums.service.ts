import { Injectable } from '@angular/core';
import { Enum } from '../models/enum.model';

@Injectable({
  providedIn: 'root'
})
export class EnumsService   {
  public destinatarioAviso: Enum[];
  public estado: Enum[];
  public estadoCivil: Enum[];
  public sexo: Enum[];
  public situacaoEntidadeConsignataria: Enum[];
  public tipoOperacaoSistema: Enum[];
  public situacaoUsuario: Enum[];
  public motivoBloqueio: Enum[];
  public motivoBloqueioServidor: Enum[];
  public produtoConsignado: Enum[];
  public situacaoProposta: Enum[];
  public situacaoConsignacao: Enum[];
  public situacaoCartao: Enum[];
  public situacaoPortabilidade: Enum[];
  public tipoRegistroPortabilidade: Enum[];
}
