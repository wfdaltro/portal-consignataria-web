export const ENUMS =  {
  situacaoEntidadeConsignataria: "SituacaoEntidadeConsignataria",
  estado: "Estado",
  motivoBloqueio: "MotivoBloqueioEntidadeConsignataria",
  sexo: "Sexo",
  estadoCivil: "EstadoCivil",
  tipoOperacaoSistema: "TipoOperacaoSistema",
  situacaoUsuario: "SituacaoUsuario",
  destinatarioAviso: "DestinatarioAviso",
  motivoBloqueioServidor: "MotivoBloqueioServidor",
  produtoConsignado: "ProdutoConsignado",
  situacaoProposta: "SituacaoProposta",
  situacaoConsignacao: "SituacaoConsignacao",
  situacaoCartao: "SituacaoCartao",
  situacaoPortabilidade: "SituacaoPortabilidade",
  tipoRegistroPortabilidade: "TipoRegistroPortabilidade",

}
