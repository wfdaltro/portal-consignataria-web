export const DATATABLE_PT = {
  "decimal":        ",",
  "emptyTable":     "Nenhum registro encontrado",
  "info":           "Mostrando _START_ até _END_ de _TOTAL_ registros",
  "infoEmpty":      "Mostrando 0 até 0 de 0 registros",
  "infoFiltered":   "(filtered from _MAX_ total entries)",
  "infoPostFix":    "",
  "thousands":      ".",
  "lengthMenu":     "Mostrar _MENU_ registros por página",
  "loadingRecords": "Carregando...",
  "processing":     "Processando...",
  "search":         "Pesquisa:",
  "zeroRecords":    "Nenhum registro encontrado",
  "paginate": {
      "first":      "<<",
      "last":       ">>",
      "next":       ">",
      "previous":   "<"
  },
  "aria": {
      "sortAscending":  ": activate to sort column ascending",
      "sortDescending": ": activate to sort column descending"
  }
}
