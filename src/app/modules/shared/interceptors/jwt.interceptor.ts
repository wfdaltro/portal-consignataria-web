import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import {SessionService} from "../services/session.service";


@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private session: SessionService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const tokenJwt = this.session.getSessionToken();
        if (tokenJwt) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${tokenJwt}`
                }
            });
        }
        return next.handle(request);
    }
}
