import {throwError} from 'rxjs';
import { CompileShallowModuleMetadata } from '@angular/compiler';

export class ErrorHandler {

    public static handleError (error: any){
        console.log(error);
        let errorMessage: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body['error'] || JSON.stringify(body);
            errorMessage = `${error.url}: ${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errorMessage = error.message ? error.message : error.toString();
        }
        return throwError(errorMessage);
    }

}
