import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';
import { ToastService } from 'src/app/modules/shared/services/toast.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  erro: boolean = undefined;

  constructor(private formBuilder: FormBuilder, private router: Router, private loginService: LoginService) {
   }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      usuario: ['', Validators.required],
      senha: ['',  Validators.required],
    });

  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.loginService.login(this.f.usuario.value, this.f.senha.value).subscribe((res)=>{
      this.loginService.obterDadosUsuario(this.f.usuario.value,).subscribe((res) =>{
        this.router.navigate(['/pages/dashboard']);
      });
    },(error)=>{
      console.log(error)
     this.erro = true;
    })
  }

}
