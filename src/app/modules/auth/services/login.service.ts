import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { SessionService } from '../../shared/services/session.service';
import { ErrorHandler } from '../../shared/exceptions/error.handler';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    private httpOptions  =  { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin':'*'})}

    constructor(private http: HttpClient, private session: SessionService, private router: Router,) { }

    login(usuario: string, senha: string) : Observable<any> {

        let dadosLogin = {
            "usuario": usuario,
            "senha": senha
        }
        return this.http.post<any>(`${environment.apiUrl}/login`, dadosLogin, this.httpOptions).pipe(
            map((res)=>{
                this.session.setSessionToken(res['authorization']);
            }),
            catchError(ErrorHandler.handleError)
        )
    }

    obterDadosUsuario(usuario: string) {
        return this.http.get<any>(`${environment.apiUrl}/sessionInfo/`, this.httpOptions).pipe(
            map((res)=>{
                console.log('aqui o retorno:' , res);
                this.session.setUserInfo(res);
                console.log( 'gravou', this.session.getUserInfo());
            }),
            catchError(ErrorHandler.handleError)
        )

    }

    logout() {
        this.session.clear();
    }

}
