import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LembrarSenhaComponent } from './pages/lembrar-senha/lembrar-senha.component';
import { LoginComponent } from './pages/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';


const routes: Routes = [
  {path: '', redirectTo: '/auth/login', pathMatch: 'full' },
  {path: 'login', component: LoginComponent,   pathMatch: 'full'},
  {path: 'lembrarSenha', component: LembrarSenhaComponent,   pathMatch: 'full'}
];

@NgModule({
  declarations: [LoginComponent, LembrarSenhaComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],

})
export class AuthenticationModule { }


