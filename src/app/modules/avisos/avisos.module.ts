import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultaAvisosComponent } from './pages/consulta-avisos/consulta-avisos.component';
import { CadastraAtualizaAvisosComponent } from './pages/cadastra-atualiza-avisos/cadastra-atualiza-avisos.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { NgxMaskModule } from 'ngx-mask';
import { DataTablesModule } from 'angular-datatables';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { CadastraAtualizaCategoriaAvisoComponent } from './pages/cadastra-atualiza-categoria-aviso/cadastra-atualiza-categoria-aviso.component';
import { ConsultaCategoriaAvisoComponent } from './pages/consulta-categoria-aviso/consulta-categoria-aviso.component';


const routes: Routes = [
  {path: '', component: ConsultaAvisosComponent,   pathMatch: 'full'},
  {path: 'categorias', component: ConsultaCategoriaAvisoComponent,   pathMatch: 'full'},
  {path: 'novo', component: CadastraAtualizaAvisosComponent,   pathMatch: 'full'},
  {path: 'atualiza/:id', component: CadastraAtualizaAvisosComponent,   pathMatch: 'full'},
  {path: 'categorias/novo', component: CadastraAtualizaCategoriaAvisoComponent,   pathMatch: 'full'},
  {path: 'categorias/atualiza/:id', component: CadastraAtualizaCategoriaAvisoComponent,   pathMatch: 'full'},

];

@NgModule({
  declarations: [ConsultaAvisosComponent, CadastraAtualizaAvisosComponent, ConsultaCategoriaAvisoComponent ,CadastraAtualizaCategoriaAvisoComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgxMaskModule.forRoot(),
    DataTablesModule,
    NgbModule,
    NgSelectModule,
  ]
})
export class AvisosModule { }
