import { Injectable } from '@angular/core';
import { HttpService } from '../../shared/services/http.service';
import { Aviso } from '../models/aviso.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AvisoService {

  private path: string = "avisos";

  constructor(private http: HttpService<Aviso, number>) {
  }

  findOne(id: number): Observable<Aviso> {
    return this.http.getOne(this.path, id);
  }

  findAll(): Observable<Aviso[]> {
    return this.http.get(this.path);
  }

  saveOrUpdate(model: Aviso, id: number) : Observable<Aviso>{
    console.log(id);
     if (id){
       return this.update(id, model);
     }
     return this.save(model);
  }

  save(model: Aviso): Observable<Aviso> {
    return this.http.post(this.path, model);
  }

  update(id: number, model: Aviso): Observable<Aviso> {
    return this.http.patch(this.path, id, model);
  }


}
