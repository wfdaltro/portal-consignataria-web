import { Injectable } from '@angular/core';
import { HttpService } from '../../shared/services/http.service';
import { CategoriaAviso } from '../models/categoria-aviso.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoriaAvisoService {

  private path: string = "categoriasAviso";

  constructor(private http: HttpService<CategoriaAviso, number>) {
  }

  findOne(id: number): Observable<CategoriaAviso> {
    return this.http.getOne(this.path, id);
  }

  findAll(): Observable<CategoriaAviso[]> {
    return this.http.get(this.path);
  }

  saveOrUpdate(model: CategoriaAviso, id: number) : Observable<CategoriaAviso>{
    console.log(id);
     if (id){
       return this.update(id, model);
     }
     return this.save(model);
  }

  save(model: CategoriaAviso): Observable<CategoriaAviso> {
   return this.http.post(this.path, model);
  }

  update(id: number, model: CategoriaAviso): Observable<CategoriaAviso> {
    return this.http.patch(this.path, id, model);
  }

}
