import {CategoriaAviso } from './categoria-aviso.model';
import { Enum } from '../../shared/models/enum.model';

export class Aviso{
  pk: number;
  versao:number;
  titulo: string;
  mensagem: string;
  categoria: CategoriaAviso;
  destinatarioAviso: Enum;
  ativo: boolean
}
