import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastService } from 'src/app/modules/shared/components/toast/toast.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ValidateBrService } from 'angular-validate-br';
import { CategoriaAviso } from '../../models/categoria-aviso.model';
import { CategoriaAvisoService } from '../../services/categoria-aviso.service';

@Component({
  selector: 'app-cadastra-atualiza-categoria-aviso',
  templateUrl: './cadastra-atualiza-categoria-aviso.component.html',
  styleUrls: ['./cadastra-atualiza-categoria-aviso.component.scss']
})
export class CadastraAtualizaCategoriaAvisoComponent implements OnInit {

  public operacao: string;
  categoriaAviso: CategoriaAviso;
  formCadastro: FormGroup;
  submitted: boolean;
  id: any;

  constructor(private formBuilder: FormBuilder, private service: CategoriaAvisoService ,private toastService: ToastService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.operacao = "Cadastrar";
    if (this.id){
      this.operacao = "Atuallizar";
    }
    this.categoriaAviso= new CategoriaAviso();
    this.formCadastro  = this.createForm(this.categoriaAviso);
    if (this.id){
      this.carregarDados();
    }
  }

  carregarDados() {
    this.service.findOne(this.id).subscribe((res)=>{
      this.formCadastro.patchValue(res);
    })
  }

  public get form() {
    return this.formCadastro.controls;
  }

  protected createForm(fields: CategoriaAviso): FormGroup {
    return this.formBuilder.group({
      pk: new FormControl(fields.pk ),
      nome: new FormControl(fields.nome, [Validators.required]),
      ativo: new FormControl(fields.ativo),
    });
  }

  public saveOrUpdate(){
    this.formCadastro.markAllAsTouched();
    this.submitted = true;
    if (this.formCadastro.invalid) {
      return;
    }
    this.service.saveOrUpdate(this.formCadastro.value, this.id).subscribe((res) => {
      console.log(res);
      this.router.navigate(['pages/avisos/categorias']);
    });
  }


}
