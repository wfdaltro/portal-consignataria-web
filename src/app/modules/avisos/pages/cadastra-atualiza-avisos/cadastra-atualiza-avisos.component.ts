import { Component, OnInit } from '@angular/core';
import { Aviso } from '../../models/aviso.model';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { AvisoService } from '../../services/aviso.service';
import { ToastService } from 'src/app/modules/shared/components/toast/toast.service';
import { Router, ActivatedRoute } from '@angular/router';
import { EnumsService } from 'src/app/modules/shared/services/enums.service';
import { Enum } from 'src/app/modules/shared/models/enum.model';
import { CategoriaAviso } from '../../models/categoria-aviso.model';
import { CategoriaAvisoService } from '../../services/categoria-aviso.service';

@Component({
  selector: 'app-cadastra-atualiza-avisos',
  templateUrl: './cadastra-atualiza-avisos.component.html',
  styleUrls: ['./cadastra-atualiza-avisos.component.scss']
})
export class CadastraAtualizaAvisosComponent implements OnInit {

  public operacao: string;
  aviso: Aviso;
  formCadastro: FormGroup;
  submitted: boolean;
  id: any;
  destinatarios : Enum[];
  categorias: CategoriaAviso[];

  constructor(private formBuilder: FormBuilder, private service: AvisoService ,private toastService: ToastService,
    private router: Router, private route: ActivatedRoute, private enumsService : EnumsService, private categoriaAvisoService: CategoriaAvisoService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.destinatarios = this.enumsService.destinatarioAviso;
    this.categoriaAvisoService.findAll().subscribe(res => {
      this.categorias = res;
    })
    this.operacao = "Cadastrar";
    if (this.id){
      this.operacao = "Atuallizar";
    }
    this.aviso = new Aviso();
    this.formCadastro  = this.createForm(this.aviso);
    if (this.id){
      this.carregarDados();
    }
  }

  carregarDados() {
    this.service.findOne(this.id).subscribe((res)=>{
      this.formCadastro.patchValue(res);
    })
  }

  public get form() {
    return this.formCadastro.controls;
  }

  protected createForm(fields: Aviso): FormGroup {
    return this.formBuilder.group({
      pk: new FormControl(fields.pk ),
      titulo: new FormControl(fields.titulo ),
      mensagem: new FormControl(fields.mensagem ),
      categoria: new FormControl(fields.categoria),
      destinatarioAviso: new FormControl(fields.destinatarioAviso ),
      ativo: new FormControl(fields.ativo  ),
    });
  }



  public saveOrUpdate(){
    this.formCadastro.markAllAsTouched();
    this.submitted = true;
    if (this.formCadastro.invalid) {
      return;
    }
    this.service.saveOrUpdate(this.formCadastro.value, this.id).subscribe((res) => {
      console.log(res);
      this.router.navigate(['pages/avisos']);

    });
  }


}
