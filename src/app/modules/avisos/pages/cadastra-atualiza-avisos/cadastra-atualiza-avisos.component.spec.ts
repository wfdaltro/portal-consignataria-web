import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastraAtualizaAvisosComponent } from './cadastra-atualiza-avisos.component';

describe('CadastraAtualizaAvisosComponent', () => {
  let component: CadastraAtualizaAvisosComponent;
  let fixture: ComponentFixture<CadastraAtualizaAvisosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastraAtualizaAvisosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastraAtualizaAvisosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
