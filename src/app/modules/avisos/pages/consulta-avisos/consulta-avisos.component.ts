import { Component, OnInit } from '@angular/core';
import { AvisoService } from '../../services/aviso.service';
import { Aviso } from '../../models/aviso.model';
import { Subject } from 'rxjs';
import { Enum } from 'src/app/modules/shared/models/enum.model';
import { Router } from '@angular/router';
import { EnumsService } from 'src/app/modules/shared/services/enums.service';
import { DATATABLE_PT } from 'src/app/modules/shared/constantes/datatable';
import { CategoriaAviso } from '../../models/categoria-aviso.model';
import { CategoriaAvisoService } from '../../services/categoria-aviso.service';

@Component({
  selector: 'app-consulta-avisos',
  templateUrl: './consulta-avisos.component.html',
  styleUrls: ['./consulta-avisos.component.scss']
})
export class ConsultaAvisosComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  avisos: Aviso[];
  filtroPesquisa: any = {};
  destinatarios : Enum[];
  categorias: CategoriaAviso[];

  constructor(private router: Router, private service: AvisoService, private enumsService : EnumsService, private categoriaAvisoService: CategoriaAvisoService) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language: DATATABLE_PT
    };
    this.destinatarios = this.enumsService.destinatarioAviso;
    this.categoriaAvisoService.findAll().subscribe(res => {
      this.categorias = res;
    })
    this.carregarDados();
  }

 carregarDados() {
    this.service.findAll().subscribe((res)=>{
      this.avisos = res;
      this.dtTrigger.next();
    },(error)=>{
      console.log(error);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }



  public detalhar(aviso: Aviso){
    this.router.navigate([`/pages/avisos/atualiza/${aviso.pk}`]);
  }


}
