import { Component, OnInit } from '@angular/core';
import { DATATABLE_PT } from 'src/app/modules/shared/constantes/datatable';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { CategoriaAviso } from '../../models/categoria-aviso.model';
import { CategoriaAvisoService } from '../../services/categoria-aviso.service';

@Component({
  selector: 'app-consulta-categoria-aviso',
  templateUrl: './consulta-categoria-aviso.component.html',
  styleUrls: ['./consulta-categoria-aviso.component.scss']
})
export class ConsultaCategoriaAvisoComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  categoriasAviso: CategoriaAviso[];

  constructor(private router: Router, private service: CategoriaAvisoService) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language: DATATABLE_PT
    };
    this.carregarDados();
  }

 carregarDados() {
    this.service.findAll().subscribe((res)=>{
      this.categoriasAviso = res;
      this.dtTrigger.next();
    },(error)=>{
      console.log(error);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }


  public detalhar(categoriasAviso: CategoriaAviso){
    this.router.navigate([`/pages/avisos/categorias/atualiza/${categoriasAviso.pk}`]);
  }


}
