import { Faq } from './faq.model';

export class CategoriaFaq{

  categoria: string;
  perguntas: Faq[];
}
