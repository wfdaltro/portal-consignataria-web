import { Component, OnInit } from '@angular/core';
import { CategoriaFaq } from '../../models/categoria.model';
import { FaqsService } from '../../services/faqs.service';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.component.html',
  styleUrls: ['./faqs.component.scss']
})
export class FaqsComponent implements OnInit {

  categorias: CategoriaFaq[];

  categoria: CategoriaFaq;

  constructor(private faqsService: FaqsService) { }

  ngOnInit(): void {
    this.faqsService.findAll().subscribe((res)=>{
      console.log(res);
      this.categorias = res;
    });
  }

}
