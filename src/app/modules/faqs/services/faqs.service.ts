import { Injectable } from '@angular/core';
import { CategoriaFaq } from '../models/categoria.model';
import { HttpService } from '../../shared/services/http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FaqsService {


  private path: string = "perguntasFrequentes";

  constructor(private http: HttpService<CategoriaFaq, number>) {
  }

  findAll() : Observable<CategoriaFaq[]> {
    return this.http.getResource(this.path);
  }


}
