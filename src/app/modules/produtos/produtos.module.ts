import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { ConsultaProdutosComponent } from './pages/consulta-produtos/consulta-produtos.component';
import { CadastroProdutosComponent } from './pages/cadastro-produtos/cadastro-produtos.component';
import { AtualizaProdutosComponent } from './pages/atualiza-produtos/atualiza-produtos.component';
import { NgxMaskModule } from 'ngx-mask';
import { DataTablesModule } from 'angular-datatables';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { CartaoComponent } from './pages/components/cartao/cartao.component';
import { EmprestimoComponent } from './pages/components/emprestimo/emprestimo.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { RenegociacaoEmprestimoComponent } from './pages/components/renegociacao-emprestimo/renegociacao-emprestimo.component';

const routes: Routes = [
  {path: '', component: ConsultaProdutosComponent,   pathMatch: 'full'},
  {path: 'novo/:controle', component: CadastroProdutosComponent,   pathMatch: 'full'},
  {path: 'atualiza/:id/:produto', component: AtualizaProdutosComponent,   pathMatch: 'full'},
];

@NgModule({
  declarations: [ConsultaProdutosComponent, CadastroProdutosComponent, AtualizaProdutosComponent, CartaoComponent, EmprestimoComponent, RenegociacaoEmprestimoComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgxMaskModule.forRoot(),
    DataTablesModule,
    NgbModule,
    NgSelectModule,
    NgxDropzoneModule,
  ]
})
export class ProdutosModule { }
