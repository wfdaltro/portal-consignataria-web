import { ProdutoConsignataria } from './produto-consignataria.model';

export class ConfiguracaoCartaoConsignataria{
  pk: number;
	juros: number;
  jurosAnual: number;
  percentualSaqueCartao: number;
  multipladorLimiteCartao: number;
  produto: ProdutoConsignataria;
}
