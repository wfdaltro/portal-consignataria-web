import { ProdutoConsignataria } from './produto-consignataria.model';

export class ConfiguracaoEmprestimoConsignataria{
  pk: number;
 parcelas: number;
 juros: number;
 cet: number;
 produto: ProdutoConsignataria;
}
