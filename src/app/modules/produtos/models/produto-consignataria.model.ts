import { Enum } from '../../shared/models/enum.model';
import { Consignataria } from '../../consignatarias/models/consignataria.model';

export class ProdutoConsignataria{
  pk: number;
  produto: Enum;
  entidadeConsignataria: Consignataria;
  ativo: boolean ;
}
