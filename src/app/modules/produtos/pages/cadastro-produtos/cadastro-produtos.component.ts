import { Component, OnInit } from '@angular/core';
import { EnumsService } from 'src/app/modules/shared/services/enums.service';
import { Enum } from 'src/app/modules/shared/models/enum.model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cadastro-produtos',
  templateUrl: './cadastro-produtos.component.html',
  styleUrls: ['./cadastro-produtos.component.scss']
})
export class CadastroProdutosComponent implements OnInit {

  produtos: Enum[];

  produtoSelecionado: Enum;

  controle: string;

  constructor(private enumsService: EnumsService, private router: Router, private route: ActivatedRoute,) { }

  ngOnInit(): void {
    this.controle = this.route.snapshot.paramMap.get('controle');
    let valores = this.controle .split("");
    console.log(valores);
    this.produtos = this.enumsService.produtoConsignado;
    this.produtos = this.enumsService.produtoConsignado.filter(pc =>{
      return ((pc.codigo === 'R' && valores[2] === '0') || (pc.codigo === 'C' && valores[0] === '0') || (pc.codigo === 'E' && valores[1] === '0'));
    })

  }

}
