import { Component, OnInit } from '@angular/core';
import { DATATABLE_PT } from 'src/app/modules/shared/constantes/datatable';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { ProdutoConsignataria } from '../../models/produto-consignataria.model';
import { ProdutosService } from 'src/app/modules/produtos/services/produtos.service';

@Component({
  selector: 'app-consulta-produtos',
  templateUrl: './consulta-produtos.component.html',
  styleUrls: ['./consulta-produtos.component.scss']
})
export class ConsultaProdutosComponent implements OnInit {


produtos: ProdutoConsignataria[];
  semProdutos: boolean = true;

  constructor(private router: Router, private service: ProdutosService) { }

  ngOnInit() {
    this.carregarDados();
  }

 carregarDados() {
    this.service.findAll().subscribe((res)=>{
      this.produtos = res;
      if (res.length > 0){
        this.semProdutos = false;
      }
    },(error)=>{
      console.log(error);
    });
  }

  public novo(){
    let c = '0';
    let e = '0';
    let r = '0';
    let controle;
    this.produtos.forEach(p=>{
      if (p.produto.codigo === 'C'){
        c = '1';
      }
      if (p.produto.codigo === 'E'){
        e = '1';
      }
      if (p.produto.codigo === 'R'){
        r = '1';
      }
      controle = c+e+r;
    })


    this.router.navigate([`/pages/produtos/novo/${controle}`]);
  }

  public edita(produto: ProdutoConsignataria){
    this.router.navigate([`/pages/produtos/atualiza/${produto.pk}/${produto.produto.codigo}`]);
  }

}
