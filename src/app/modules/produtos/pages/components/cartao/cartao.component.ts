import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastService } from 'src/app/modules/shared/components/toast/toast.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProdutosService } from 'src/app/modules/produtos/services/produtos.service';
import { ConfiguracaoCartaoConsignataria } from 'src/app/modules/produtos/models/configuracao-cartao-consignataria.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-configuracao-cartao',
  templateUrl: './cartao.component.html',
  styleUrls: ['./cartao.component.scss']
})
export class CartaoComponent implements OnInit {

  configuracaoCartaoConsignataria: ConfiguracaoCartaoConsignataria;
  formCadastro: FormGroup;
  submitted: boolean;

  @Input() produto: string;
  id: number;

  constructor(private formBuilder: FormBuilder, private toastService: ToastService,
    private router: Router, private route: ActivatedRoute, private service: ProdutosService, private toastr: ToastrService,) { }

  ngOnInit(): void {
    console.log('produto:', this.produto);
    this.id =  +this.produto;
    this.configuracaoCartaoConsignataria = new ConfiguracaoCartaoConsignataria();
    this.formCadastro  = this.createForm(this.configuracaoCartaoConsignataria);
    if (this.id){
      console.log(this.id);
      this.carregarDados();
    }
  }

  carregarDados() {
    this.service.findOneCartao(this.id).subscribe((res)=>{
      this.formCadastro.patchValue(res);
    })
  }
  public get form() {
    return this.formCadastro.controls;
  }

  protected createForm(fields: ConfiguracaoCartaoConsignataria): FormGroup {
    return this.formBuilder.group({
      pk: new FormControl(fields.pk),
      multipladorLimiteCartao: new FormControl(fields.multipladorLimiteCartao, [Validators.required]),
      juros: new FormControl(fields.juros, [Validators.required] ),
      jurosAnual: new FormControl(fields.jurosAnual, [Validators.required]),
      percentualSaqueCartao: new FormControl(fields.percentualSaqueCartao, [Validators.required]),
    });
  }

  public saveOrUpdate(){
    this.formCadastro.markAllAsTouched();
    this.submitted = true;
    if (this.formCadastro.invalid) {
      return;
    }
    let cartao  =  Object.assign({}, this.formCadastro.value);
    this.service.saveOrUpdateCartao(cartao, this.id).subscribe((res) => {
      console.log(res);
      this.toastr.success('Configurações salvas com sucesso!','', {
        timeOut: 3000
      });
      this.router.navigate([`/pages/produtos/`]);
    }, (err: any) =>{
      this.toastr.error(err,'Erro ao processar a requisição', {
        timeOut: 3000
      });
    });
  }

}
