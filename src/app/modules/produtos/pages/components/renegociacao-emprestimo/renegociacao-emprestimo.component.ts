import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { ConfiguracaoEmprestimoConsignataria } from '../../../models/configuracao-emprestimo-consignado.model';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ProdutosService } from '../../../services/produtos.service';

@Component({
  selector: 'app-configuracao-renegociacao-emprestimo',
  templateUrl: './renegociacao-emprestimo.component.html',
  styleUrls: ['./renegociacao-emprestimo.component.scss']
})
export class RenegociacaoEmprestimoComponent implements OnInit {

  @Input() produto: string;
  configuracoesParcelas: any[] = [];
  files: File[] = [];
  fileName: string = "Selecione o arquivo";
  modelRef: any;
  id: number;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private service: ProdutosService,
    private toastr: ToastrService,
    private modalService: NgbModal) { }

    ngOnInit() {
      this.id =  +this.produto;
      if (this.id){
        this.carregarDados();
      }
    }

    carregarDados() {
      this.service.findRenegociacoes(this.id).subscribe((res)=>{
          if(res){
            this.configuracoesParcelas = res;
          }
        });
    }

    onSelect(event) {
      this.files = event.addedFiles;
    }

    onRemove(event) {
      this.files.splice(this.files.indexOf(event), 1);
    }

  importarArquivo(){
    console.log('meu arquivo:',   this.files[0])
    this.service.importarParcelasRenegociacao(this.files[0]).then((res)=>{
      this.configuracoesParcelas = res.body;
      this.files = [];
      this.modelRef.close();
    },(err)=>{
      this.toastr.error(err,'Erro ao processar a requisição', {
        timeOut: 3000
      });
    });
  }

  removeParcela(parcela: number): void {
    let index = this.configuracoesParcelas.findIndex(p => p.parcelas === parcela);
    this.configuracoesParcelas.splice(index, 1);
  }

  adicionarParcela():void{
    this.configuracoesParcelas.push({});
  }

  openModal(modal: any) {
    this.modelRef = this.modalService.open(modal, { centered: true });
  }

  onFileChange(file: File) {
   this.fileName  = file.name;
  }

  saveOrUpdate(): void{
    if(!this.configuracoesParcelas || this.configuracoesParcelas.length === 0){
      this.toastr.error('Informe ao menos uma configuração de parcela', 'Erro ', {
        timeOut: 3000
      });
      return;
    }
    console.log('Pacelas a serm salvas:', this.configuracoesParcelas);
      this.service.saveOrUpdateRenegocicoes(this.configuracoesParcelas, this.id).subscribe((res)=>{
        this.toastr.success('Configurações salvas com sucesso!','', {
          timeOut: 3000
        });
        this.router.navigate([`/pages/produtos/`]);
      }, (err: any) =>{
        this.toastr.error(err,'Erro ao processar a requisição', {
          timeOut: 3000
        });
      });
  }

}
