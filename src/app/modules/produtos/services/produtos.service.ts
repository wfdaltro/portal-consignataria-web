import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ConfiguracaoCartaoConsignataria } from 'src/app/modules/produtos/models/configuracao-cartao-consignataria.model';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ErrorHandler } from '../../shared/exceptions/error.handler';
import { ProdutoConsignataria } from '../models/produto-consignataria.model';
import { ConfiguracaoEmprestimoConsignataria } from '../models/configuracao-emprestimo-consignado.model';

@Injectable({
  providedIn: 'root'
})
export class ProdutosService {

  private path: string = "produtoConsignataria";

  private apiUrl: string = environment.apiUrl;

  private  headers: HttpHeaders;

  public httpOptions: {
      headers: HttpHeaders,
  };

  constructor(protected http: HttpClient) {
      this.httpOptions = { headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*'
      })};
  }

  findAll():  Observable<ProdutoConsignataria[]>{
    return this.http.get<ProdutoConsignataria[]>( `${this.apiUrl}/${this.path}`, this.httpOptions).pipe(
      catchError(ErrorHandler.handleError)
    );
    }

  // Cartão

  findOneCartao(id: any):  Observable<ConfiguracaoCartaoConsignataria>{
    return this.http.get<ConfiguracaoCartaoConsignataria>(`${this.apiUrl}/${this.path}/cartao/${id}`, this.httpOptions).pipe(
      catchError(ErrorHandler.handleError)
    );
  }

  saveCartao(model: ConfiguracaoCartaoConsignataria): Observable<ConfiguracaoCartaoConsignataria> {
    return this.http.post<ConfiguracaoCartaoConsignataria>(`${this.apiUrl}/${this.path}/cartao`, model, this.httpOptions).pipe(
      catchError(ErrorHandler.handleError)
    );

  }

  saveOrUpdateCartao(model: ConfiguracaoCartaoConsignataria, id: number) : Observable<ConfiguracaoCartaoConsignataria>{
    console.log(id);
     if (id){
       return this.updateCartao(id, model);
     }
     return this.saveCartao(model);
  }

  updateCartao(id: number, model: ConfiguracaoCartaoConsignataria): Observable<ConfiguracaoCartaoConsignataria> {
    return this.http.patch<ConfiguracaoCartaoConsignataria>(`${this.apiUrl}/${this.path}/cartao`, model, this.httpOptions).pipe(
      catchError(ErrorHandler.handleError)
  );
  }

  // Empréstimos


  findEmprestimos(id: any):  Observable<ConfiguracaoEmprestimoConsignataria[]>{
    return this.http.get<ConfiguracaoEmprestimoConsignataria[]>(`${this.apiUrl}/${this.path}/emprestimo/${id}`, this.httpOptions).pipe(
      catchError(ErrorHandler.handleError)
    );
  }

  saveEmprestimos(model: ConfiguracaoEmprestimoConsignataria[]): Observable<ConfiguracaoEmprestimoConsignataria[]> {
    return this.http.post<ConfiguracaoEmprestimoConsignataria[]>(`${this.apiUrl}/${this.path}/emprestimo`, model, this.httpOptions).pipe(
      catchError(ErrorHandler.handleError)
    );

  }

  saveOrUpdateEmprestimos(model: ConfiguracaoEmprestimoConsignataria[], id: number) : Observable<ConfiguracaoEmprestimoConsignataria[]>{
    console.log(id);
     if (id){
       return this.updateEmprestimos(id, model);
     }
     return this.saveEmprestimos(model);
  }

  updateEmprestimos(id: number, model: ConfiguracaoEmprestimoConsignataria[]): Observable<ConfiguracaoEmprestimoConsignataria[]> {
    return this.http.patch<ConfiguracaoEmprestimoConsignataria[]>(`${this.apiUrl}/${this.path}/emprestimo/${id}`, model, this.httpOptions).pipe(
      catchError(ErrorHandler.handleError)
    );
  }

  

  // Renegociacao


  findRenegociacoes(id: any):  Observable<ConfiguracaoEmprestimoConsignataria[]>{
    return this.http.get<ConfiguracaoEmprestimoConsignataria[]>(`${this.apiUrl}/${this.path}/renegociacao/${id}`, this.httpOptions).pipe(
      catchError(ErrorHandler.handleError)
    );
  }

  saveRenegociacoes(model: ConfiguracaoEmprestimoConsignataria[]): Observable<ConfiguracaoEmprestimoConsignataria[]> {
    return this.http.post<ConfiguracaoEmprestimoConsignataria[]>(`${this.apiUrl}/${this.path}/renegociacao`, model, this.httpOptions).pipe(
      catchError(ErrorHandler.handleError)
    );

  }

  saveOrUpdateRenegocicoes(model: ConfiguracaoEmprestimoConsignataria[], id: number) : Observable<ConfiguracaoEmprestimoConsignataria[]>{
    console.log(id);
     if (id){
       return this.updateRenegociacoes(id, model);
     }
     return this.saveRenegociacoes(model);
  }

  updateRenegociacoes(id: number, model: ConfiguracaoEmprestimoConsignataria[]): Observable<ConfiguracaoEmprestimoConsignataria[]> {
    return this.http.patch<ConfiguracaoEmprestimoConsignataria[]>(`${this.apiUrl}/${this.path}/renegociacao/${id}`, model, this.httpOptions).pipe(
      catchError(ErrorHandler.handleError)
    );
  }


  //

  importarParcelas(file: File): Promise<any> {
    console.log('file a importar:', file);
    let formData: FormData = new FormData();
    formData.append('file', file);
    const req = new HttpRequest('POST', `${this.apiUrl}/${this.path}/emprestimo/uploadArquivoParcelas`, formData, {
      reportProgress: true,
      responseType: 'json'
    });
    console.log('req:', req);
    console.log('formData:', formData);
    return this.http.request(req).toPromise();
  }

  importarParcelasRenegociacao(file: File): Promise<any> {
    console.log('file a importar:', file);
    let formData: FormData = new FormData();
    formData.append('file', file);
    const req = new HttpRequest('POST', `${this.apiUrl}/${this.path}/renegociacao/uploadArquivoParcelas`, formData, {
      reportProgress: true,
      responseType: 'json'
    });
    console.log('req:', req);
    console.log('formData:', formData);
    return this.http.request(req).toPromise();
  }


}
