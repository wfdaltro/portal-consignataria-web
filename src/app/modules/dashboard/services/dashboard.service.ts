import { Injectable } from '@angular/core';
import { HttpService } from '../../shared/services/http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  private pathCartao: string = "dashboard";

  constructor(private http: HttpService<any, any>) {
  }

  obterAvisos(): Observable<any[]>{
    return this.http.getResource(`${this.pathCartao}/avisos`);
  }

}
