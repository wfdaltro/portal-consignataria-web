import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../services/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashBoardComponent implements OnInit {


  avisos: any[]

  constructor(private dashboardService: DashboardService) {

  }

  ngOnInit() {
    this.dashboardService.obterAvisos().subscribe(res=>{
      console.log(res);
      this.avisos = res;
    });
  }

}
