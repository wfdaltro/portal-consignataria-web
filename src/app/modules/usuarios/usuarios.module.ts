import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultaUsuarioComponent } from './pages/consulta-usuario/consulta-usuario.component';
import { CadastroUsuarioComponent } from './pages/cadastro-usuario/cadastro-usuario.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';

const routes: Routes = [
  {path: '', component: ConsultaUsuarioComponent,   pathMatch: 'full'},
  {path: 'novo', component: CadastroUsuarioComponent,   pathMatch: 'full'},
  {path: 'atualiza/:id', component: CadastroUsuarioComponent,   pathMatch: 'full'},
];


@NgModule({
  declarations: [ConsultaUsuarioComponent, CadastroUsuarioComponent],
  imports: [

  CommonModule,
  RouterModule.forChild(routes),
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  SharedModule,
  NgxMaskModule.forRoot(),
  DataTablesModule,
  NgbModule,
  NgSelectModule,

  ]
})
export class UsuarioModule { }
