import { Injectable } from '@angular/core';
import { HttpService } from '../../shared/services/http.service';
import { OperadorEntidadeConsignataria } from '../models/operador-consignataria.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  private path: string = "operadoresConsignataria";

  constructor(private http: HttpService<OperadorEntidadeConsignataria, number>) {
  }

  findOne(id: number): Observable<OperadorEntidadeConsignataria> {
    return this.http.getOne(this.path, id);
  }


  findByParams(params?: any): Observable<OperadorEntidadeConsignataria[]> {
    return this.http.getParams(this.path + '/search', params);
  }


  save(model: OperadorEntidadeConsignataria): Observable<OperadorEntidadeConsignataria> {
    return this.http.post(this.path, model);
  }

  update(id: number, model: OperadorEntidadeConsignataria): Observable<OperadorEntidadeConsignataria> {
    return this.http.patch(this.path, id, model);
  }

}
