import { Enum } from '../../shared/models/enum.model';
import { Consignataria } from '../../consignatarias/models/consignataria.model';

export class OperadorEntidadeConsignataria {
 pk:number;
 nome:string;
 cpf:string;
 email:string;
 telefone:string;
 matricula:string;
 enderecoIp:string;
 entidadeConsignataria: Consignataria;
 master:boolean;
 login:string;
 senha:string;
 dataExpiracaoSenha:string;
 situacao: Enum;
 pkConsignataria: number;
}
