import { Component, OnInit } from '@angular/core';
import { OperadorEntidadeConsignataria } from '../../models/operador-consignataria.model';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastService } from 'src/app/modules/shared/components/toast/toast.service';
import { Router, ActivatedRoute } from '@angular/router';
import { EnumsService } from 'src/app/modules/shared/services/enums.service';
import { UsuariosService } from '../../services/usuarios.service';
import { ValidateBrService } from 'angular-validate-br';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cadastro-usuario',
  templateUrl: './cadastro-usuario.component.html',
  styleUrls: ['./cadastro-usuario.component.scss']
})
export class CadastroUsuarioComponent implements OnInit {

  usuario: OperadorEntidadeConsignataria;s
  formCadastro: FormGroup;
  submitted: boolean;
  comboMaster = [{'descricao':'Sim', 'valor':true}, {'descricao':'Não', 'valor':false}]
  id: any;
  acao: string

  constructor(private formBuilder: FormBuilder, private service: UsuariosService ,private toastService: ToastService,
    private router: Router, private route: ActivatedRoute, private enumsService : EnumsService,
    private validateBrService: ValidateBrService, private toastr: ToastrService, ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.usuario = new OperadorEntidadeConsignataria();
    this.formCadastro  = this.createForm(this.usuario);
    this.acao = 'Cadastrar';
    if (this.id){
      this.acao = 'Atualizar';
      this.carregarDados();
    }
  }
  carregarDados() {
    this.service.findOne(this.id).subscribe((res)=>{
      this.formCadastro.patchValue(res);
    })
  }
  public get form() {
    return this.formCadastro.controls;
  }

  protected createForm(fields: OperadorEntidadeConsignataria): FormGroup {
    return this.formBuilder.group({
      cpf: new FormControl(fields.cpf, [Validators.required, this.validateBrService.cpf]),
      email: new FormControl(fields.email, [Validators.required] ),
      matricula: new FormControl(fields.matricula, [Validators.required] ),
      nome: new FormControl(fields.nome, [Validators.required]),
      telefone: new FormControl(fields.telefone),
      master: new FormControl(fields.master),
      enderecoIp: new FormControl(fields.enderecoIp),
    });
  }



  public saveOrUpdate(){
    this.formCadastro.markAllAsTouched();
    this.submitted = true;
    if (this.formCadastro.invalid) {
      return;
    }
    let operador  =  Object.assign({}, this.formCadastro.value);
    this.service.save(operador).subscribe((res) => {
      this.toastr.success('Registro salvo com sucesso!', 'Sucesso');
      this.router.navigate([`/pages/usuarios/`]);
    },(err)=>{
      this.toastr.error('Ocorreu um erro ao salvar o registro. ' + err, 'Erro ao processar a requisição');
    });
  }

  cancelar(){
    this.router.navigate([`/pages/usuarios`]);
  }

}
