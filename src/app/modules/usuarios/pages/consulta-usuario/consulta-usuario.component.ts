import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { OperadorEntidadeConsignataria } from 'src/app/modules/usuarios/models/operador-consignataria.model';
import { Consignataria } from 'src/app/modules/consignatarias/models/consignataria.model';
import { Router, ActivatedRoute } from '@angular/router';
import { ConsignatariasService } from 'src/app/modules/consignatarias/services/consignatarias.service';
import { DATATABLE_PT } from 'src/app/modules/shared/constantes/datatable';
import { UsuariosService } from '../../services/usuarios.service';

@Component({
  selector: 'app-consulta-usuario',
  templateUrl: './consulta-usuario.component.html',
  styleUrls: ['./consulta-usuario.component.scss']
})
export class ConsultaUsuarioComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  usuarios: OperadorEntidadeConsignataria[];
  consignataria: Consignataria;
  filtroPesquisa: any = {};
  semAcesso: boolean;

  constructor(private router: Router, private route: ActivatedRoute,
    private service: ConsignatariasService, private usuarioService: UsuariosService) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language: DATATABLE_PT
    };
    this.usuarioService.findByParams({}).subscribe((res)=>{
      this.usuarios = res ;
      this.dtTrigger.next();
      if (this.usuarios.length == 0){
        this.semAcesso = true;
      }
    });
  }

  public pesquisar(): void {
    this.usuarioService.findByParams(this.filtroPesquisa).subscribe((res)=>{
      this.usuarios = res || [];
    },(error)=>{
      console.log(error);
    });
  }

  public edita(usuario: OperadorEntidadeConsignataria){
    this.router.navigate([`/pages/usuarios/atualiza/${usuario.pk}`]);
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

}
