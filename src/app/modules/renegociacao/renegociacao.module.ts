import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultaEmprestimosRenegociacaoComponent } from './pages/consulta-emprestimos-renegociacao/consulta-emprestimos-renegociacao.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { NgxMaskModule } from 'ngx-mask';
import { DataTablesModule } from 'angular-datatables';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { NgxCurrencyModule } from 'ngx-currency';
import { RenegociacaoEmprestimosComponent } from './pages/renegociacao-emprestimos/renegociacao-emprestimos.component';
import { ConfirmacaoRenegociacaoEmprestimoComponent } from './pages/confirmacao-rengociacao-emprestimo/confirmacao-renegociacao-emprestimo.component';


const routes: Routes = [
  {path: '', component: ConsultaEmprestimosRenegociacaoComponent,   pathMatch: 'full'},
  {path: 'renegociar/:idServidor', component: RenegociacaoEmprestimosComponent,   pathMatch: 'full'},
  {path: 'confirmacao/:idServidor', component: ConfirmacaoRenegociacaoEmprestimoComponent,   pathMatch: 'full'},
];


@NgModule({
  declarations: [ConsultaEmprestimosRenegociacaoComponent, RenegociacaoEmprestimosComponent, ConfirmacaoRenegociacaoEmprestimoComponent],
  imports: [

  RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgxMaskModule.forRoot(),
    DataTablesModule,
    NgbModule,
    NgSelectModule,
    NgxDropzoneModule,
    NgxCurrencyModule
  ]
})
export class RenegociacaoModule { }
