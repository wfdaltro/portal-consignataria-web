import { Component, OnInit } from '@angular/core';
import { Servidor } from 'src/app/modules/servidores/models/servidor.model';
import { Router } from '@angular/router';
import { ServidoresService } from 'src/app/modules/servidores/services/servidores.service';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { EmprestimoConsignado } from 'src/app/modules/consignacoes/models/emprestimo-consignado.model';
import { DATATABLE_PT } from 'src/app/modules/shared/constantes/datatable';
import { ConsignacoesService } from 'src/app/modules/consignacoes/services/consignacoes.service';

@Component({
  selector: 'app-consulta-emprestimos-renegociacao',
  templateUrl: './consulta-emprestimos-renegociacao.component.html',
  styleUrls: ['./consulta-emprestimos-renegociacao.component.scss']
})
export class ConsultaEmprestimosRenegociacaoComponent implements OnInit {

  chave: string;
  servidor: Servidor;
  fase = 1;
  selecionados: any[];

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  emprestimos: EmprestimoConsignado[];

  constructor(private router: Router, private servidoresService: ServidoresService, private consignacoesService: ConsignacoesService,
    private toastr: ToastrService, ) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language: DATATABLE_PT
    };
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  pesquisarServidor(){
    if (!this.chave){
      this.toastr.error('Informe o CPF ou a matrícula do Servidor.','Erro ao processar a requisição', {
        timeOut: 3000
      });
      return;
    }
    this.servidoresService.findByCpfMatricula(this.chave).subscribe((res)=>{
      this.servidor = res;
    });
  }

  public pesquisarEmprestimos(): void {
    this.consignacoesService.findByParams(this.servidor.pk).subscribe((res)=>{
      this.emprestimos = res;
    },(error)=>{
      console.log(error);
    });
  }

  iniciarRenegociacao(){
      this.fase = 2;
      this.pesquisarEmprestimos();
  }

  renegociar(){
   const ids: number[] =  this.emprestimos.filter(emprestimo => emprestimo['selecionado']).map(emprestimo => emprestimo.pk);
   this.router.navigate([`/pages/renegociacao/renegociar/${this.servidor.pk}`],  { queryParams: { ids: ids }});  
  }

}
       