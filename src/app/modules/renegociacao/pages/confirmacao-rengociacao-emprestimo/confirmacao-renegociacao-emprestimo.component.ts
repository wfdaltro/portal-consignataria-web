 
import { RenegociacaoService } from '../../services/renegociacao.service';
import { Component, OnInit } from '@angular/core';
import { Servidor } from 'src/app/modules/servidores/models/servidor.model';
import { OfertaEmprestimo } from 'src/app/modules/propostas/models/oferta-emprestimo.model';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { ServidoresService } from 'src/app/modules/servidores/services/servidores.service';

@Component({
  selector: 'app-confirmacao-renegociacao-emprestimo',
  templateUrl: './confirmacao-renegociacao-emprestimo.component.html',
  styleUrls: ['./confirmacao-renegociacao-emprestimo.component.scss']
})
export class ConfirmacaoRenegociacaoEmprestimoComponent implements OnInit {

  servidor: Servidor;
  oferta: OfertaEmprestimo;
  enviada: boolean = false;
  observacoes: string;
  propostaCriada: any;
  dadosSimulacao: any = {};


  constructor(private propostaService: RenegociacaoService, private toastr: ToastrService,
    private router: Router, private route: ActivatedRoute, private servidoresService: ServidoresService) { }

  ngOnInit(): void {
    const idServidor = this.route.snapshot.paramMap.get('idServidor');
    this.route.queryParamMap .subscribe((params) => {
      this.dadosSimulacao = {...params };
      this.servidoresService.findOne(+idServidor).subscribe((res)=>{
        this.servidor = res;
        this.simular();
      });
    });

  }

  salvarProposta(){
    this.propostaService.salvarPropostaRenegociacao(this.servidor, this.oferta, this.observacoes, this.dadosSimulacao.params.emprestimos).subscribe((res)=>{
      this.enviada = true;
      this.propostaCriada = res;
    },(err)=>{
      this.toastr.error('Ocorreu um erro ao criar a proposta:' + err,'Erro ao processar a requisição', {
        timeOut: 3000
      });
    });
  }


  simular(){
    const dadosSimulacao = this.dadosSimulacao.params
    if (dadosSimulacao.prestacao){
      this.simularPorParcela(dadosSimulacao);
      return;
    }
    if (dadosSimulacao.totalFinanciado){
      this.simularPorValorFinanciado(dadosSimulacao);
      return;
    }
  }

  simularPorParcela(dadosSimulacao: any){
    this.propostaService.gerarPropostaEmprestimoParcela(this.servidor.pk, dadosSimulacao).subscribe((res)=>{
      this.oferta = res;
    });
  }

  simularPorValorFinanciado(dadosSimulacao: any){
    this.propostaService.gerarPropostaEmprestimoValorFinanciado(this.servidor.pk, dadosSimulacao).subscribe((res)=>{
      this.oferta = res;
    });
  }

}
