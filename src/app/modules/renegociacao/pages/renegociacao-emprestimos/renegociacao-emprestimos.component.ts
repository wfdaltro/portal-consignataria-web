import { Component, OnInit } from '@angular/core';
import { EmprestimoConsignado } from 'src/app/modules/consignacoes/models/emprestimo-consignado.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ServidoresService } from 'src/app/modules/servidores/services/servidores.service';
import { ConsignacoesService } from 'src/app/modules/consignacoes/services/consignacoes.service';
import { ToastrService } from 'ngx-toastr';
import { Servidor } from 'src/app/modules/servidores/models/servidor.model';
import { RenegociacaoService } from '../../services/renegociacao.service';

@Component({
  selector: 'app-renegociacao-emprestimos',
  templateUrl: './renegociacao-emprestimos.component.html',
  styleUrls: ['./renegociacao-emprestimos.component.scss']
})
export class RenegociacaoEmprestimosComponent implements OnInit {

  dadosSimulacao: any = {};

  emprestimos: EmprestimoConsignado[] = [];

  oferta: any;

  servidor: Servidor;

  constructor( private toastr: ToastrService,
    private router: Router,private route: ActivatedRoute,  
    private servidoresService: ServidoresService,
    private consignacoesService: ConsignacoesService,
    private renegociacaoService: RenegociacaoService,
 ) { }

  ngOnInit() {
    this.dadosSimulacao['totalFinanciado'] = 0.0;

    const id = this.route.snapshot.paramMap.get('idServidor');
    this.servidoresService.findOne(+id).subscribe((res)=>{
      this.servidor = res;
    });

    this.route.queryParams.subscribe(params => {
        params['ids'].forEach(element => {
            this.consignacoesService.findOne(element).subscribe(res =>{
              this.dadosSimulacao['totalFinanciado'] += res.totalSaldoResidual;
              this.emprestimos.push(res);
            });
        });
         
      }
    );
  }

  simular(){

    if (! this.dadosSimulacao.prestacao && !this.dadosSimulacao.totalFinanciado){
      this.toastr.error('Infome o valor do financiamento ou o valor das parcelas.','Erro ao processar a requisição', {
        timeOut: 3000
      });
      return;
    }
    if ( this.dadosSimulacao.prestacao && this.dadosSimulacao.totalFinanciado){
      this.toastr.error('Infome somente um dos valores.','Erro ao processar a requisição', {
        timeOut: 3000
      });
      return;
    }
    if (!this.dadosSimulacao.meses ){
      this.toastr.error('Infome a quantidade de parcelas.','Erro ao processar a requisição', {
        timeOut: 3000
      });
      return;
    }
    if (this.dadosSimulacao.prestacao){
      this.simularPorParcela();
      return;
    }
    if (this.dadosSimulacao.totalFinanciado){
      this.simularPorValorFinanciado();
      return;
    }
  }

  simularPorParcela(){
    this.renegociacaoService.gerarPropostaEmprestimoParcela(this.servidor.pk, this.dadosSimulacao).subscribe((res)=>{
      this.oferta = res;
    });
  }

  simularPorValorFinanciado(){
    this.renegociacaoService.gerarPropostaEmprestimoValorFinanciado(this.servidor.pk, this.dadosSimulacao).subscribe((res)=>{
      this.oferta = res;
    });
  }

  aceitarProposta(){
      let emprestimos = ""
      this.emprestimos.forEach((emp)=>{
        emprestimos += emp.ade + ";"
      })
      this.router.navigate([`/pages/renegociacao/confirmacao/${this.servidor.pk}`], { queryParams: { prestacao: this.dadosSimulacao.prestacao, totalFinanciado: this.dadosSimulacao.totalFinanciado, meses:this.dadosSimulacao.meses, emprestimos: emprestimos } });
  }


}
