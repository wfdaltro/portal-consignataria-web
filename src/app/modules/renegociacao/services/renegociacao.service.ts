import { Injectable } from '@angular/core';
import { HttpService } from '../../shared/services/http.service';
import { Observable } from 'rxjs';
import { OfertaEmprestimo } from '../../propostas/models/oferta-emprestimo.model';
import { Servidor } from '../../servidores/models/servidor.model';

@Injectable({
  providedIn: 'root'
})
export class RenegociacaoService {

  private pathRenegociacao: string = "renegociacaoEmprestimo";

  constructor(private http: HttpService<any, any>) {
  }

  gerarPropostaEmprestimoParcela(pk: number, dadosSimulacao:any): Observable<OfertaEmprestimo>{
    console.log(`${this.pathRenegociacao}/simular/valorParcela?idServidor=${pk}&valor=${dadosSimulacao.prestacao}&parcelas=${dadosSimulacao.parcelas}`);
    return this.http.getResource(`${this.pathRenegociacao}/simular/valorParcela?idServidor=${pk}&valor=${dadosSimulacao.prestacao}&parcelas=${dadosSimulacao.meses}`);
  }

  gerarPropostaEmprestimoValorFinanciado(pk: number, dadosSimulacao: any): Observable<OfertaEmprestimo>{
    console.log(`${this.pathRenegociacao}/simular/valorFinanciamento?idServidor=${pk}&valor=${dadosSimulacao.totalFinanciado}&parcelas=${dadosSimulacao.parcelas}`);
    return this.http.getResource(`${this.pathRenegociacao}/simular/valorFinanciamento?idServidor=${pk}&valor=${dadosSimulacao.totalFinanciado}&parcelas=${dadosSimulacao.meses}`);
  }

  salvarPropostaRenegociacao(servidor: Servidor, oferta: OfertaEmprestimo, observacoes: string, emprestimos: string) : Observable<any> {
    let proposta: any = {};
    proposta.jurosMes  = oferta.jurosMensal;
    proposta.observacoes = observacoes;
    proposta.parcelas = oferta.parcelas;
    proposta.valorPrestacao = oferta.valorParcela;
    proposta.valorTotal = oferta.valorTotal;
    proposta.valorLiberado = oferta.valorEmprestimo;
    proposta.emprestimos = emprestimos;
    proposta.servidor = {
      pk: servidor.pk
    } as Servidor

    console.log(proposta);
    return this.http.post(this.pathRenegociacao ,proposta);
  }

}
