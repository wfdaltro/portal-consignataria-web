import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultaCartoesComponent } from './pages/consulta-cartoes/consulta-cartoes.component';
import { DetalhaCartoesComponent } from './pages/detalha-cartoes/detalha-cartoes.component';
import { GerenciaAnexosCartoesComponent } from './pages/gerencia-anexos-cartoes/gerencia-anexos-cartoes.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { NgxMaskModule } from 'ngx-mask';
import { DataTablesModule } from 'angular-datatables';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDropzoneModule } from 'ngx-dropzone';

const routes: Routes = [
  {path: '', component: ConsultaCartoesComponent,   pathMatch: 'full'},
  {path: ':id', component: DetalhaCartoesComponent,   pathMatch: 'full'},
  {path: ':id/anexos', component: GerenciaAnexosCartoesComponent,   pathMatch: 'full'},

];


@NgModule({
  declarations: [ConsultaCartoesComponent, DetalhaCartoesComponent, GerenciaAnexosCartoesComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgxMaskModule.forRoot(),
    DataTablesModule,
    NgbModule,
    NgSelectModule,
     NgxDropzoneModule,
  ]
})
export class CartoesModule { }
