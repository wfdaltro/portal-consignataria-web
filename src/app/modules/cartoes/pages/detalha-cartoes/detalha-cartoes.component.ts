import { Component, OnInit } from '@angular/core';
import { CartaoConsignado } from '../../models/cartao-consignado.model';
import { CartoesService } from '../../services/cartoes.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detalha-cartoes',
  templateUrl: './detalha-cartoes.component.html',
  styleUrls: ['./detalha-cartoes.component.scss']
})
export class DetalhaCartoesComponent implements OnInit {

  cartao: CartaoConsignado;

  constructor(private service: CartoesService, private toastr: ToastrService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.service.findOne(+id).subscribe((res)=>{
      this.cartao = res;
    });
  }



  anexos(){
    this.router.navigate([`/pages/cartoes/${this.cartao.pk}/anexos`]);
  }

}
