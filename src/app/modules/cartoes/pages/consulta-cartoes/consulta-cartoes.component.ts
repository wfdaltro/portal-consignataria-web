import { Component, OnInit } from '@angular/core';
import { CartaoConsignado } from '../../models/cartao-consignado.model';
import { Subject } from 'rxjs';
import { Enum } from 'src/app/modules/shared/models/enum.model';
import { Consignataria } from 'src/app/modules/consignatarias/models/consignataria.model';
import { Router } from '@angular/router';
import { CartoesService } from '../../services/cartoes.service';
import { EnumsService } from 'src/app/modules/shared/services/enums.service';
import { ConsignatariasService } from 'src/app/modules/consignatarias/services/consignatarias.service';
import { DATATABLE_PT } from 'src/app/modules/shared/constantes/datatable';
import { EmprestimoConsignado } from 'src/app/modules/consignacoes/models/emprestimo-consignado.model';
import { Servidor } from 'src/app/modules/servidores/models/servidor.model';
import { ServidoresService } from 'src/app/modules/servidores/services/servidores.service';

@Component({
  selector: 'app-consulta-cartoes',
  templateUrl: './consulta-cartoes.component.html',
  styleUrls: ['./consulta-cartoes.component.scss']
})
export class ConsultaCartoesComponent implements OnInit {


  cartoes: CartaoConsignado[];
  filtroPesquisa: any = {};
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  situacoes: Enum[]
  servidores: Servidor[];

  constructor(private router: Router, private cartoesService: CartoesService,
    private enumsService: EnumsService, private servidoresService: ServidoresService) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language: DATATABLE_PT
    };
    this.carregarDados();
    this.carregarServidores();
    this.carregarSituacoes();
  }

  carregarServidores() {
    this.servidoresService.findByParams().subscribe((res)=>{
      this.servidores = res
    });
   }

   carregarSituacoes() {
     this.situacoes =  this.enumsService.situacaoCartao;
   }

 carregarDados() {
    this.cartoesService.findByParams(this.filtroPesquisa).subscribe((res)=>{
      this.cartoes = res;
      console.log(this.cartoes);
      this.dtTrigger.next();
    },(error)=>{
      console.log(error);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  public pesquisar(): void {
    this.cartoesService.findByParams(this.filtroPesquisa).subscribe((res)=>{
      this.cartoes = res;
    },(error)=>{
      console.log(error);
    });
  }

   detalhar(cartao: CartaoConsignado){
    this.router.navigate([`/pages/cartoes/${cartao.pk}`]);
  }

  parcelas(cartao: CartaoConsignado){
    this.router.navigate([`/pages/cartoes/${cartao.pk}/parcelas`]);
  }

  anexos(cartao: CartaoConsignado){
    this.router.navigate([`/pages/cartoes/${cartao.pk}/anexos`]);
  }

}
