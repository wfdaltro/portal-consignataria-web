import { Component, OnInit } from '@angular/core';
import { CartaoConsignado } from '../../models/cartao-consignado.model';
import { CartoesService } from '../../services/cartoes.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Documento } from 'src/app/modules/consignacoes/models/documento-emprestimo.model';

@Component({
  selector: 'app-gerencia-anexos-cartoes',
  templateUrl: './gerencia-anexos-cartoes.component.html',
  styleUrls: ['./gerencia-anexos-cartoes.component.scss']
})
export class GerenciaAnexosCartoesComponent implements OnInit {

  files: File = null;
  fileName: string ;
  documentos: any[];
  emprestimo: CartaoConsignado;
  modelRef: any;
  label: string = 'Selecione um arquivo'


  constructor(private service: CartoesService, private toastr: ToastrService,
    private router: Router, private route: ActivatedRoute,  private modalService: NgbModal) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.service.findOne(+id).subscribe((res)=>{
      this.emprestimo = res;
      this.carregarDocumentos();
    });

  }

  carregarDocumentos(){
    this.service.carregarDocumentos(this.emprestimo.pk).subscribe((res)=>{
      this.documentos = res;
    });
  }



  onSelect(files: FileList) {
    console.log(files)
    this.files = files.item(0);
    this.label = this.files.name
}

  download(documento: Documento){
    this.service.download(documento.pk).subscribe((res)=>{
      console.log('resposta:', res['type']);
      let blob:any = new Blob([res], { type: res['type'] });
      const url= window.URL.createObjectURL(blob);
      window.open(url);
    });
  }

  adicionarArquivos(){
    this.service.adicionarDocumentos(this.emprestimo.pk, this.fileName, this.files).subscribe((res)=>{
      this.carregarDocumentos();
      this.modelRef.close();
      this.label = 'Selecione um arquivo.'
      this.fileName= null;
    });
  }

  openModal(modal: any) {
    this.fileName = null;
    this.files = null;
    this.modelRef = this.modalService.open(modal, { centered: true });
  }


}
