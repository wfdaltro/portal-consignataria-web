import { Consignataria } from 'src/app/modules/consignatarias/models/consignataria.model';
import { Enum } from '../../shared/models/enum.model';
import { Servidor } from '../../servidores/models/servidor.model';

export class CartaoConsignado {

pk:number;

	 servidor: Servidor;


	 consignataria: Consignataria;


	 situacao: Enum;


 ade: string;


	 jurosMes: number;


	    valorLimiteCartao: number;


	    percentualSaqueCartao: number;


	    valorSaqueCartao: number;


	    jurosAno: number;


}
