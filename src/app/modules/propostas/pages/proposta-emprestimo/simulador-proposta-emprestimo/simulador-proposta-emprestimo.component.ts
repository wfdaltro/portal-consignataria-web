import { Component, OnInit } from '@angular/core';
import { OfertaCartao } from '../../../models/oferta-cartao.model';
import { OfertaEmprestimo } from '../../../models/oferta-emprestimo.model';
import { PropostaService } from '../../../services/proposta.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { Servidor } from '../../../../servidores/models/servidor.model';
import { ServidoresService } from '../../../../servidores/services/servidores.service';

@Component({
  selector: 'app-simulador-proposta-emprestimo',
  templateUrl: './simulador-proposta-emprestimo.component.html',
  styleUrls: ['./simulador-proposta-emprestimo.component.scss']
})
export class SimuladorPropostaEmprestimoComponent implements OnInit {

  dadosSimulacao: any = {};

  oferta: OfertaEmprestimo;

  servidor: Servidor;

  semProposta: boolean = false;

  constructor(private propostaService: PropostaService, private toastr: ToastrService,
    private router: Router, private route: ActivatedRoute, private servidoresService: ServidoresService) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('idServidor');
    this.servidoresService.findOne(+id).subscribe((res)=>{
      this.servidor = res;
    });
  }

  simular(){

    if (! this.dadosSimulacao.prestacao && !this.dadosSimulacao.totalFinanciado){
      this.toastr.error('Infome o valor do financiamento ou o valor das parcelas.','Erro ao processar a requisição', {
        timeOut: 3000
      });
      return;
    }
    if ( this.dadosSimulacao.prestacao && this.dadosSimulacao.totalFinanciado){
      this.toastr.error('Infome somente um dos valores.','Erro ao processar a requisição', {
        timeOut: 3000
      });
      return;
    }
    if (!this.dadosSimulacao.meses ){
      this.toastr.error('Infome a quantidade de parcelas.','Erro ao processar a requisição', {
        timeOut: 3000
      });
      return;
    }
    if (this.dadosSimulacao.prestacao){
      this.simularPorParcela();
      return;
    }
    if (this.dadosSimulacao.totalFinanciado){
      this.simularPorValorFinanciado();
      return;
    }
  }

  simularPorParcela(){
    this.propostaService.gerarPropostaEmprestimoParcela(this.servidor.pk, this.dadosSimulacao).subscribe((res)=>{
      this.oferta = res;
    },(err)=>{
      this.semProposta = true;
      this.oferta = null;
    });
  }

  simularPorValorFinanciado(){
    this.propostaService.gerarPropostaEmprestimoValorFinanciado(this.servidor.pk, this.dadosSimulacao).subscribe((res)=>{
      this.oferta = res;
    },(err)=>{
      this.semProposta = true;
      this.oferta = null;
    });
  }

  aceitarProposta(){
      this.router.navigate([`/pages/propostas/emprestimo/confirmacao/${this.servidor.pk}`], { queryParams: { prestacao: this.dadosSimulacao.prestacao, totalFinanciado: this.dadosSimulacao.totalFinanciado, meses:this.dadosSimulacao.meses } });
  }

}
