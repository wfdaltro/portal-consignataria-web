import { Component, OnInit } from '@angular/core';
import { Servidor } from 'src/app/modules/servidores/models/servidor.model';
import { PropostaService } from '../../services/proposta.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-proposta-emprestimo',
  templateUrl: './proposta-emprestimo.component.html',
  styleUrls: ['./proposta-emprestimo.component.scss']
})
export class PropostaEmprestimoComponent implements OnInit {

  chave: string;
  servidor: Servidor;


  constructor(private propostaCartaoService: PropostaService, private router: Router,) { }

  ngOnInit(): void {
  }

  pesquisarServidor(){
    this.propostaCartaoService.pesquisarServidor(this.chave).subscribe((res)=>{
      this.servidor = res;
    });
  }

  simulacao(){
    this.router.navigate([`pages/propostas/emprestimo/simulador/${this.servidor.pk}`]);
  }

}
