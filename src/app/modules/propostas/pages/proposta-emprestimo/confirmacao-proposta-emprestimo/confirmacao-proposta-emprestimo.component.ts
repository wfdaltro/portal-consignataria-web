  import { Component, OnInit } from '@angular/core';
import { PropostaEmprestimoComponent } from '../proposta-emprestimo.component';
import { PropostaEmprestimoConsignado } from '../../../models/proposta-emprestimo.model';
import { Servidor } from 'src/app/modules/servidores/models/servidor.model';
import { OfertaEmprestimo } from '../../../models/oferta-emprestimo.model';
import { PropostaService } from '../../../services/proposta.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { ServidoresService } from '../../../../servidores/services/servidores.service';

@Component({
  selector: 'app-confirmacao-proposta-emprestimo',
  templateUrl: './confirmacao-proposta-emprestimo.component.html',
  styleUrls: ['./confirmacao-proposta-emprestimo.component.scss']
})
export class ConfirmacaoPropostaEmprestimoComponent implements OnInit {

  servidor: Servidor;
  oferta: OfertaEmprestimo;
  enviada: boolean = false;
  observacoes: string;
  propostaCriada: PropostaEmprestimoConsignado;
  dadosSimulacao: any = {};


  constructor(private propostaService: PropostaService, private toastr: ToastrService,
    private router: Router, private route: ActivatedRoute, private servidoresService: ServidoresService) { }

  ngOnInit(): void {
    const idServidor = this.route.snapshot.paramMap.get('idServidor');
    this.route.queryParamMap .subscribe((params) => {
      this.dadosSimulacao = {...params };
      this.servidoresService.findOne(+idServidor).subscribe((res)=>{
        this.servidor = res;
        this.simular();
      });
    });

  }

  salvarProposta(){
    this.propostaService.salvarPropostaEmprestimo(this.servidor, this.oferta, this.observacoes).subscribe((res)=>{
      this.enviada = true;
      this.propostaCriada = res;
    },(err)=>{
      this.toastr.error(err,'Erro ao processar a requisição', {
        timeOut: 3000
      });
    });
  }


  simular(){
    const dadosSimulacao = this.dadosSimulacao.params
    if (dadosSimulacao.prestacao){
      this.simularPorParcela(dadosSimulacao);
      return;
    }
    if (dadosSimulacao.totalFinanciado){
      this.simularPorValorFinanciado(dadosSimulacao);
      return;
    }
  }

  simularPorParcela(dadosSimulacao: any){
    this.propostaService.gerarPropostaEmprestimoParcela(this.servidor.pk, dadosSimulacao).subscribe((res)=>{
      this.oferta = res;
    });
  }

  simularPorValorFinanciado(dadosSimulacao: any){
    this.propostaService.gerarPropostaEmprestimoValorFinanciado(this.servidor.pk, dadosSimulacao).subscribe((res)=>{
      this.oferta = res;
    });
  }

}
