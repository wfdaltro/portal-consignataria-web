import { Component, OnInit } from '@angular/core';
import { PropostaEmprestimoConsignado } from '../../models/proposta-emprestimo.model';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { PropostaService } from '../../services/proposta.service';
import { DATATABLE_PT } from 'src/app/modules/shared/constantes/datatable';
import { Enum } from 'src/app/modules/shared/models/enum.model';
import { Servidor } from 'src/app/modules/servidores/models/servidor.model';
import { ServidoresService } from '../../../servidores/services/servidores.service';
import { EnumsService } from 'src/app/modules/shared/services/enums.service';

@Component({
  selector: 'app-consulta-propostas-emprestimo',
  templateUrl: './consulta-propostas-emprestimo.component.html',
  styleUrls: ['./consulta-propostas-emprestimo.component.scss']
})
export class ConsultaPropostasEmprestimoComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  propostas: PropostaEmprestimoConsignado[];
  filtroPesquisa: any = {};
  situacoes: Enum[]
  servidores: Servidor[];

  constructor(private router: Router, private service: PropostaService,
      private enumsService: EnumsService, private servidoresService: ServidoresService) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language: DATATABLE_PT
    };
    this.carregarDados();
    this.carregarServidores();
    this.carregarSituacoes();
  }

  carregarServidores() {
   this.servidoresService.findByParams().subscribe((res)=>{
     this.servidores = res
   });
  }

  carregarSituacoes() {
    this.situacoes =  this.enumsService.situacaoProposta;
  }

 carregarDados() {
    this.service.findPropostasEmprestimoByParams(this.filtroPesquisa).subscribe((res)=>{
      this.propostas = res;
      this.dtTrigger.next();
    },(error)=>{
      console.log(error);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  public pesquisar(): void {
    this.service.findPropostasEmprestimoByParams(this.filtroPesquisa).subscribe((res)=>{
      this.propostas = res;
    },(error)=>{
      console.log(error);
    });
  }

  public detalhar(proposta: PropostaEmprestimoConsignado){
    console.log(proposta);
    this.router.navigate([`/pages/propostas/emprestimo/detalha/${proposta.pk}`]);
  }


}
