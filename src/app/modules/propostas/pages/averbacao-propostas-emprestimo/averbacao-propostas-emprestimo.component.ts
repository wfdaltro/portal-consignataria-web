import { Component, OnInit } from '@angular/core';
import { PropostaEmprestimoConsignado } from '../../models/proposta-emprestimo.model';
import { PropostaService } from '../../services/proposta.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-averbacao-propostas-emprestimo',
  templateUrl: './averbacao-propostas-emprestimo.component.html',
  styleUrls: ['./averbacao-propostas-emprestimo.component.scss']
})
export class AverbacaoPropostasEmprestimoComponent implements OnInit {

  proposta: PropostaEmprestimoConsignado;

  tokenAverbacao: string;

  constructor(private propostaService: PropostaService, private toastr: ToastrService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    const idProposta = this.route.snapshot.paramMap.get('id');
    this.propostaService.findOneProposta(+idProposta).subscribe((res)=>{
      this.proposta = res;
    });
  }

  confirmarDeferimento(){
    if (!this.tokenAverbacao){
      this.toastr.error('Informe o token de averbação da proposta.','Erro ao processar a requisição', {
        timeOut: 3000
      });
      return;
    }


    this.propostaService.averbarPropostaEmprestimo(this.proposta.pk, this.tokenAverbacao).subscribe((res)=>{
      this.toastr.success('Proposta averbada com sucesso.', 'Sucesso!', {
        timeOut: 3000
      });
      this.router.navigate([`/pages/propostas/emprestimo/consulta`]);
    },(err)=>{
      this.toastr.error(err,'Erro ao processar a requisição', {
        timeOut: 3000
      });
    });
  }

  cancelar(){
    this.router.navigate([`/pages/propostas/emprestimo/detalha/${this.proposta.pk}`]);
  }

}
