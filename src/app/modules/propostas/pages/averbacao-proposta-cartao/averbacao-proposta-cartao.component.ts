import { Component, OnInit } from '@angular/core';
import { PropostaCartao } from '../../models/proposta-cartao.model';
import { PropostaService } from '../../services/proposta.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-averbacao-proposta-cartao',
  templateUrl: './averbacao-proposta-cartao.component.html',
  styleUrls: ['./averbacao-proposta-cartao.component.scss']
})
export class AverbacaoPropostaCartaoComponent implements OnInit {

  proposta: PropostaCartao;

  tokenAverbacao: string;

  constructor(private propostaService: PropostaService, private toastr: ToastrService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    const idProposta = this.route.snapshot.paramMap.get('id');
    this.propostaService.findOnePropostaCartao(+idProposta).subscribe((res)=>{
      this.proposta = res;
      console.log(res);
    });
  }

  confirmarDeferimento(){
    if (!this.tokenAverbacao){
      this.toastr.error('Informe o token de averbação da proposta.','Erro ao processar a requisição', {
        timeOut: 3000
      });
      return;
    }


    this.propostaService.averbarPropostaCartao(this.proposta.pk, this.tokenAverbacao).subscribe((res)=>{
      this.toastr.success('Proposta averbada com sucesso.', 'Sucesso!', {
        timeOut: 3000
      });
      this.router.navigate([`/pages/propostas/cartao/consulta`]);
    },(err)=>{
      this.toastr.error(err,'Erro ao processar a requisição', {
        timeOut: 3000
      });
    });
  }

  cancelar(){
    this.router.navigate([`/pages/propostas/cartao/detalha/${this.proposta.pk}`]);
  }


}
