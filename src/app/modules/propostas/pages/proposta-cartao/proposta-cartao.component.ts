import { Component, OnInit } from '@angular/core';
import {PropostaService } from '../../services/proposta.service';
import { Servidor } from '../../../servidores/models/servidor.model';
import { OfertaCartao } from '../../models/oferta-cartao.model';
import { ToastrService } from 'ngx-toastr';
import { PropostaCartao } from '../../models/proposta-cartao.model';

@Component({
  selector: 'app-proposta-cartao',
  templateUrl: './proposta-cartao.component.html',
  styleUrls: ['./proposta-cartao.component.scss']
})
export class PropostaCartaoComponent implements OnInit {

  chave: string;
  servidor: Servidor;
  oferta: OfertaCartao;
  confirmacao: boolean = false;
  enviada: boolean = false;
  observacoes: string;
  propostaCriada: PropostaCartao;
  semProposta: boolean;
  semMargem: boolean = false;


  constructor(private propostaService: PropostaService, private toastr: ToastrService,) { }

  ngOnInit(): void {
  }

  pesquisarServidor(){
    if (!this.chave){
      this.toastr.error('Informe o CPF ou a matrícula do Servidor.','Erro ao processar a requisição', {
        timeOut: 3000
      });
      return;
    }
    this.propostaService.pesquisarServidor(this.chave).subscribe((res)=>{
      this.servidor = res;
      alert(this.servidor.margemCartaoDisponivel);
      if (this.servidor.margemCartaoDisponivel === 0.0 ){
        this.semMargem = true;
      }
    });
  }

  gerarProposta(){
    this.propostaService.gerarPropostaCartao(this.servidor.pk).subscribe((res)=>{
      this.oferta = res;
    },(err)=>{
      this.semProposta = true;
      this.oferta = null;
    });
  }

  confirmarProposta(){
    this.confirmacao = true;
  }

  salvarProposta(){
    this.propostaService.salvarPropostaCartao(this.servidor, this.oferta, this.observacoes).subscribe((res)=>{
      this.enviada = true;
      this.propostaCriada = res;
    },(err)=>{
      this.toastr.error('Ocorreu um erro ao criar a proposta:' + err,'Erro ao processar a requisição', {
        timeOut: 3000
      });
    });
  }

}
