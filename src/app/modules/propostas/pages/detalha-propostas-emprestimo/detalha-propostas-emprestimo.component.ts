import { Component, OnInit } from '@angular/core';
import { PropostaEmprestimoConsignado } from '../../models/proposta-emprestimo.model';
import { PropostaService } from '../../services/proposta.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OcorrrenciaPropostaEmprestimoConsignado } from '../../models/ocorrencia-proposta-emprestimo.model';
import { Documento } from 'src/app/modules/consignacoes/models/documento-emprestimo.model';

@Component({
  selector: 'app-detalha-propostas-emprestimo',
  templateUrl: './detalha-propostas-emprestimo.component.html',
  styleUrls: ['./detalha-propostas-emprestimo.component.scss']
})
export class DetalhaPropostasEmprestimoComponent implements OnInit {

  proposta: PropostaEmprestimoConsignado;

  ocorrencias: OcorrrenciaPropostaEmprestimoConsignado[];
  documentos : any[];

  files: File = null;
  fileName: string ;
  modelRef: any;
  label: string = 'Selecione um arquivo'

  constructor(private propostaService: PropostaService, private toastr: ToastrService,
    private router: Router, private route: ActivatedRoute, private modalService: NgbModal) { }

  ngOnInit(): void {
    const idProposta = this.route.snapshot.paramMap.get('id');
    this.propostaService.findOneProposta(+idProposta).subscribe((res)=>{
      this.proposta = res;
      this.propostaService.findOcorrenciasPropostaEmprestimo(+idProposta).subscribe((res)=>{
        this.ocorrencias = res;
        this.carregarDocumentos();
      });
    });


  }


  averbar(){
    this.router.navigate([`/pages/propostas/emprestimo/averbacao/${this.proposta.pk}`]);
  }

  indeferir(){
    this.router.navigate([`/pages/propostas/emprestimo/ocorrencia/${this.proposta.pk}/3`]);
  }

  devolver(){
    this.router.navigate([`/pages/propostas/emprestimo/ocorrencia/${this.proposta.pk}/5`]);
  }


  onSelect(files: FileList) {
    console.log(files)
    this.files = files.item(0);
    this.label = this.files.name
}

download(documento: Documento){
  this.propostaService.download(documento.pk).subscribe((res)=>{
    console.log('resposta:', res['type']);
    let blob:any = new Blob([res], { type: res['type'] });
    const url= window.URL.createObjectURL(blob);
    window.open(url);
  });
}

  adicionarArquivos(){
    this.propostaService.adicionarDocumentos(this.proposta.pk, this.fileName, this.files).subscribe((res)=>{
      this.carregarDocumentos();
      this.modelRef.close();
      this.label = 'Selecione um arquivo.'
      this.fileName= null;
    });
  }

  openModal(modal: any) {
    this.fileName = null;
    this.files = null;
    this.modelRef = this.modalService.open(modal, { centered: true });
  }

  carregarDocumentos(){
    this.propostaService.carregarDocumentos(this.proposta.pk).subscribe((res)=>{
      this.documentos = res;
    });
  }

  baixarTermoAutorizacao(){
    this.propostaService.downloadTermoDesconto(this.proposta.pk).subscribe((res)=>{
      console.log('resposta:', res['data']);
      console.log('res:', res);
     
      const url= window.URL.createObjectURL(res.data);
      window.open(url);
    });
  }

}
