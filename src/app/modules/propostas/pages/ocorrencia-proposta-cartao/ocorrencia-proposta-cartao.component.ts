import { Component, OnInit } from '@angular/core';
import { PropostaCartao } from '../../models/proposta-cartao.model';
import { PropostaService } from '../../services/proposta.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ocorrencia-proposta-cartao',
  templateUrl: './ocorrencia-proposta-cartao.component.html',
  styleUrls: ['./ocorrencia-proposta-cartao.component.scss']
})
export class OcorrenciaPropostaCartaoComponent implements OnInit {

  proposta: PropostaCartao;
  motivoOcorrencia: string;
  acao: string;
  operacao: string
  labelOperacao: string;

  constructor(private propostaService: PropostaService, private toastr: ToastrService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    const idProposta = this.route.snapshot.paramMap.get('id');
    this.acao = this.route.snapshot.paramMap.get('acao');
    if (this.acao === '3'){
      this.operacao = 'Indeferir'
      this.labelOperacao = 'do indeferimento';
    }
    if (this.acao === '5'){
      this.operacao = 'Devolver'
      this.labelOperacao = 'da devolução';
    }
    this.propostaService.findOnePropostaCartao(+idProposta).subscribe((res)=>{
      this.proposta = res;
    });
  }

  confirmarGravacaoOcorrencia(){
    if (!this.motivoOcorrencia){
      this.toastr.error(`Informe o  motivo  ${this.labelOperacao}`,'Erro ao processar a requisição', {
        timeOut: 3000
      });
      return;
    }
    this.propostaService.gravarOcorrenciaPropostaCartao(this.proposta.pk, this.acao, this.motivoOcorrencia).subscribe((res)=>{
      this.toastr.success('Proposta averbada com sucesso.', 'Sucesso!', {
        timeOut: 3000
      });
      this.router.navigate([`/pages/propostas/cartao/consulta`]);
    },(err)=>{
      this.toastr.error(err,'Erro ao processar a requisição', {
        timeOut: 3000
      });
    });
  }

  cancelar(){
    this.router.navigate([`/pages/propostas/cartao/detalha/${this.proposta.pk}`]);
  }

}
