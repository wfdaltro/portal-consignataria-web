import { Component, OnInit } from '@angular/core';
import { PropostaCartao } from '../../models/proposta-cartao.model';
import { PropostaService } from '../../services/proposta.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { OcorrenciaPropostaCartaoConsignado } from '../../models/ocorrencia-proposta-cartao.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Documento } from 'src/app/modules/consignacoes/models/documento-emprestimo.model';

@Component({
  selector: 'app-detalha-propostas-cartao',
  templateUrl: './detalha-propostas-cartao.component.html',
  styleUrls: ['./detalha-propostas-cartao.component.scss']
})
export class DetalhaPropostasCartaoComponent implements OnInit {

  proposta: PropostaCartao;
  ocorrencias: OcorrenciaPropostaCartaoConsignado[];
  documentos : any[];

  files: File = null;
  fileName: string ;
  modelRef: any;
  label: string = 'Selecione um arquivo'

  constructor(private propostaService: PropostaService, private toastr: ToastrService,
    private router: Router, private route: ActivatedRoute,  private modalService: NgbModal) { }

  ngOnInit(): void {
    const idProposta = this.route.snapshot.paramMap.get('id');
    this.propostaService.findOnePropostaCartao(+idProposta).subscribe((res)=>{
      this.proposta = res;
    });
  }


  averbar(){
    this.router.navigate([`/pages/propostas/cartao/averbacao/${this.proposta.pk}`]);
  }

  indeferir(){
    this.router.navigate([`/pages/propostas/cartao/ocorrencia/${this.proposta.pk}/3`]);
  }

  devolver(){
    this.router.navigate([`/pages/propostas/cartao/ocorrencia/${this.proposta.pk}/5`]);
  }


  onSelect(files: FileList) {
    console.log(files)
    this.files = files.item(0);
    this.label = this.files.name
}

download(documento: Documento){
  this.propostaService.download(documento.pk).subscribe((res)=>{
    console.log('resposta:', res['type']);
    let blob:any = new Blob([res], { type: res['type'] });
    const url= window.URL.createObjectURL(blob);
    window.open(url);
  });
}

  adicionarArquivos(){
    this.propostaService.adicionarDocumentos(this.proposta.pk, this.fileName, this.files).subscribe((res)=>{
      this.carregarDocumentos();
      this.modelRef.close();
      this.label = 'Selecione um arquivo.'
      this.fileName= null;
    });
  }

  openModal(modal: any) {
    this.fileName = null;
    this.files = null;
    this.modelRef = this.modalService.open(modal, { centered: true });
  }

  carregarDocumentos(){
    this.propostaService.carregarDocumentos(this.proposta.pk).subscribe((res)=>{
      this.documentos = res;
    });
  }


}
