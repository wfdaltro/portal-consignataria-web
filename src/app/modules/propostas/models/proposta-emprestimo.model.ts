import { OperadorEntidadeConsignataria } from '../../usuarios/models/operador-consignataria.model';
import { Servidor } from '../../servidores/models/servidor.model';
import { Enum } from '../../shared/models/enum.model';

export class PropostaEmprestimoConsignado  {


  pk: number;
	numeroProposta: string;

  operadorAprovacao: any;

  operadorResponsavel: any;

  servidor: Servidor;


	  valorLiberado: number;


	  valorTotal: number;


	  valorPrestacao: number;


	  parcelas: number;


	  cetMes: number;


	  cetAno: number;


	  jurosMes: number;


	  jurosAno: number;


	  totalEncargos: number;

  dataValidadeProposta: string;


  observacoes: string;

  situacao: Enum;

  consignataria: any;
}
