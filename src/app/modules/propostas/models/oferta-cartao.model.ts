export class OfertaCartao {

  limite: number
  percentualSaque: number
  valorSaqueAutorizado: number
	juros: number
	jurosAnual: number
  dataSimulacao: string

}
