import { Servidor } from '../../servidores/models/servidor.model';
import { OperadorEntidadeConsignataria } from '../../usuarios/models/operador-consignataria.model';
import { Enum } from '../../shared/models/enum.model';

export class PropostaCartao{

  pk: number;
	numeroProposta: string;


  operadorAprovacao: OperadorEntidadeConsignataria;

 operadorResponsavel: OperadorEntidadeConsignataria;

  servidor: Servidor;

 jurosMes: number;

valorLimiteCartao: number;;


percentualSaqueCartao: number;;


valorSaqueCartao: number;;


jurosAno: number;;


dataValidadeProposta: string;


  observacoes: string;

  situacao: Enum;

  consignataria: any;

}
