import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { ServidoresService } from '../../servidores/services/servidores.service';
import { HttpService } from '../../shared/services/http.service';
import { Observable } from 'rxjs';
import { OfertaCartao } from '../models/oferta-cartao.model';
import { Servidor } from '../../servidores/models/servidor.model';
import { PropostaCartao } from '../models/proposta-cartao.model';
import { OfertaEmprestimo } from '../models/oferta-emprestimo.model';
import { PropostaEmprestimoConsignado } from '../models/proposta-emprestimo.model';
import { environment } from 'src/environments/environment';
import { HttpRequest } from '@angular/common/http';
import { OcorrrenciaPropostaEmprestimoConsignado } from '../models/ocorrencia-proposta-emprestimo.model';

@Injectable({
  providedIn: 'root'
})
export class PropostaService {

   private pathCartao: string = "propostaCartao";


   private pathEmprestimo: string = "propostaEmprestimo";

  constructor(private http: HttpService<any, any>, private servidoresService: ServidoresService) {
  }

  pesquisarServidor(chave: string): Observable<Servidor> {
    return this.servidoresService.findByCpfMatricula(chave);
  }

  salvarPropostaCartao(servidor: Servidor, oferta: OfertaCartao, observacoes: string) : Observable<PropostaCartao>{
    let proposta: PropostaCartao = new PropostaCartao();
    proposta.jurosAno  = oferta.jurosAnual;
    proposta.jurosMes = oferta.juros;
    proposta.observacoes = observacoes;
    proposta.percentualSaqueCartao = oferta.percentualSaque;
    proposta.valorLimiteCartao = oferta.limite;
    proposta.valorSaqueCartao = oferta.valorSaqueAutorizado;
    proposta.servidor = {
      pk: servidor.pk
    } as Servidor
    return this.http.post(this.pathCartao ,proposta);
  }

  salvarPropostaEmprestimo(servidor: Servidor, oferta: OfertaEmprestimo, observacoes: string) : Observable<PropostaEmprestimoConsignado> {
    let proposta: PropostaEmprestimoConsignado = new PropostaEmprestimoConsignado();
    proposta.jurosMes  = oferta.jurosMensal;
    proposta.observacoes = observacoes;
    proposta.parcelas = oferta.parcelas;
    proposta.valorPrestacao = oferta.valorParcela;
    proposta.valorTotal = oferta.valorTotal;
    proposta.valorLiberado = oferta.valorEmprestimo;
    proposta.servidor = {
      pk: servidor.pk
    } as Servidor
    return this.http.post(this.pathEmprestimo ,proposta);
  }


  gerarPropostaCartao(pk: number): Observable<OfertaCartao>{
    return this.http.getResource(`${this.pathCartao}/simular?idServidor=${pk}`);
  }

  gerarPropostaEmprestimoParcela(pk: number, dadosSimulacao:any): Observable<OfertaEmprestimo>{
    console.log(`${this.pathEmprestimo}/simular/valorParcela?idServidor=${pk}&valor=${dadosSimulacao.prestacao}&parcelas=${dadosSimulacao.parcelas}`);
    return this.http.getResource(`${this.pathEmprestimo}/simular/valorParcela?idServidor=${pk}&valor=${dadosSimulacao.prestacao}&parcelas=${dadosSimulacao.meses}`);
  }

  gerarPropostaEmprestimoValorFinanciado(pk: number, dadosSimulacao: any): Observable<OfertaEmprestimo>{
    console.log(`${this.pathEmprestimo}/simular/valorFinanciamento?idServidor=${pk}&valor=${dadosSimulacao.totalFinanciado}&parcelas=${dadosSimulacao.parcelas}`);
    return this.http.getResource(`${this.pathEmprestimo}/simular/valorFinanciamento?idServidor=${pk}&valor=${dadosSimulacao.totalFinanciado}&parcelas=${dadosSimulacao.meses}`);
  }

  findPropostasEmprestimoByParams(params?: any): Observable<PropostaEmprestimoConsignado[]> {
    return this.http.getParams(this.pathEmprestimo + '/search', params);
  }

  findOneProposta(id: number): Observable<PropostaEmprestimoConsignado> {
    return this.http.getOne(this.pathEmprestimo, id);
  }

  averbarPropostaEmprestimo(id: number, token: string){
    return this.http.pathResource(id, `${this.pathEmprestimo}/averbar`, token);
  }

  gravarOcorrenciaPropostaEmprestimo(pk: number, acao: string, motivoOcorrencia: string) {
    let dadosOperacao: any = {
      observacao: motivoOcorrencia,
	    acao: acao
    }
    return this.http.pathResource(pk, `${this.pathEmprestimo}/ocorrencia`, dadosOperacao);
  }

  findPropostasCartaoByParams(params?: any): Observable<PropostaCartao[]> {
    return this.http.getParams(this.pathCartao + '/search', params);
  }

  findOnePropostaCartao(id: number): Observable<PropostaCartao> {
    return this.http.getOne(this.pathCartao, id);
  }

  gravarOcorrenciaPropostaCartao(pk: number, acao: string, motivoOcorrencia: string) {
    let dadosOperacao: any = {
      observacao: motivoOcorrencia,
	    acao: acao
    }
    return this.http.pathResource(pk, `${this.pathCartao}/ocorrencia`, dadosOperacao);
  }

  averbarPropostaCartao(id: number, token: string){
    return this.http.pathResource(id, `${this.pathCartao}/averbar`, token);
  }


  carregarDocumentos(pk: number) {
    return this.http.get(this.pathEmprestimo +"/anexos?id="+pk) ;
  }

  adicionarDocumentos(pk: number, nome: string, file: File){
    const formData = new FormData();
    formData.append("file", file);
    const req = new HttpRequest('POST', `${environment.apiUrl}/${this.pathEmprestimo}/anexo/add?idEmprestimo=${pk}&nome=${nome}`, formData, {
      reportProgress: true,
      responseType: 'json'
    });
    return this.http.http.request(req);
  }

  download(pk: number) {
    return this.http.http.get(`${environment.apiUrl}/${this.pathEmprestimo}/anexo/download?idAnexo=${pk}`, {responseType: 'blob'});
    }

    findOcorrenciasPropostaEmprestimo(id: number): Observable<OcorrrenciaPropostaEmprestimoConsignado[]> {
      return this.http.getOne(this.pathEmprestimo+ '/ocorrencias', id);
    }

    downloadTermoDesconto(pk: number) {
      return this.http.http.get(`${environment.apiUrl}/${this.pathEmprestimo}/termo/download?idProposta=${pk}`, {responseType: 'blob'})
      .pipe(map((response)=>{
        return {
          filename: 'dailyOrdersReport.pdf',
          data: new Blob([response], {type: 'application/pdf'})
        };
    }));}
 


}
