import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PropostaCartaoComponent } from './pages/proposta-cartao/proposta-cartao.component';
import { PropostaEmprestimoComponent } from './pages/proposta-emprestimo/proposta-emprestimo.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { NgxMaskModule } from 'ngx-mask';
import { DataTablesModule } from 'angular-datatables';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { SimuladorPropostaEmprestimoComponent } from './pages/proposta-emprestimo/simulador-proposta-emprestimo/simulador-proposta-emprestimo.component';
import { ConfirmacaoPropostaEmprestimoComponent } from './pages/proposta-emprestimo/confirmacao-proposta-emprestimo/confirmacao-proposta-emprestimo.component';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { NgxCurrencyModule } from 'ngx-currency';
import { ConsultaPropostasCartaoComponent } from './pages/consulta-propostas-cartao/consulta-propostas-cartao.component';
import { ConsultaPropostasEmprestimoComponent } from './pages/consulta-propostas-emprestimo/consulta-propostas-emprestimo.component';
import { DetalhaPropostasEmprestimoComponent } from './pages/detalha-propostas-emprestimo/detalha-propostas-emprestimo.component';
import { DetalhaPropostasCartaoComponent } from './pages/detalha-propostas-cartao/detalha-propostas-cartao.component';
import { AverbacaoPropostasEmprestimoComponent } from './pages/averbacao-propostas-emprestimo/averbacao-propostas-emprestimo.component';
import { OcorrenciaPropostasEmprestimoComponent } from './pages/ocorrencia-propostas-emprestimo/ocorrencia-propostas-emprestimo.component';
import { OcorrenciaPropostaCartaoComponent } from './pages/ocorrencia-proposta-cartao/ocorrencia-proposta-cartao.component';
import { AverbacaoPropostaCartaoComponent } from './pages/averbacao-proposta-cartao/averbacao-proposta-cartao.component';



const routes: Routes = [
  {path: 'emprestimo', component: PropostaEmprestimoComponent,   pathMatch: 'full'},
  {path: 'cartao', component: PropostaCartaoComponent,   pathMatch: 'full'},
  {path: 'emprestimo/simulador/:idServidor', component: SimuladorPropostaEmprestimoComponent,   pathMatch: 'full'},
  {path: 'emprestimo/confirmacao/:idServidor', component: ConfirmacaoPropostaEmprestimoComponent,   pathMatch: 'full'},
  {path: 'emprestimo/consulta', component: ConsultaPropostasEmprestimoComponent,   pathMatch: 'full'},
  {path: 'emprestimo/detalha/:id', component: DetalhaPropostasEmprestimoComponent,   pathMatch: 'full'},
  {path: 'emprestimo/averbacao/:id', component: AverbacaoPropostasEmprestimoComponent,   pathMatch: 'full'},
  {path: 'emprestimo/ocorrencia/:id/:acao', component: OcorrenciaPropostasEmprestimoComponent,   pathMatch: 'full'},

  {path: 'cartao/consulta', component: ConsultaPropostasCartaoComponent,   pathMatch: 'full'},
  {path: 'cartao/detalha/:id', component: DetalhaPropostasCartaoComponent,   pathMatch: 'full'},
  {path: 'cartao/averbacao/:id', component: AverbacaoPropostaCartaoComponent,   pathMatch: 'full'},
  {path: 'cartao/ocorrencia/:id/:acao', component: OcorrenciaPropostaCartaoComponent,   pathMatch: 'full'},
];

@NgModule({
  declarations: [PropostaCartaoComponent, PropostaEmprestimoComponent, SimuladorPropostaEmprestimoComponent,
    ConfirmacaoPropostaEmprestimoComponent, ConsultaPropostasCartaoComponent, ConsultaPropostasEmprestimoComponent,
    DetalhaPropostasEmprestimoComponent, DetalhaPropostasCartaoComponent, AverbacaoPropostasEmprestimoComponent,
    OcorrenciaPropostasEmprestimoComponent,
    OcorrenciaPropostaCartaoComponent,
    AverbacaoPropostaCartaoComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgxMaskModule.forRoot(),
    DataTablesModule,
    NgbModule,
    NgSelectModule,
    NgxCurrencyModule
  ]
})
export class PropostasModule { }
