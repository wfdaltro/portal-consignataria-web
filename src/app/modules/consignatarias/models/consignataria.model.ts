import { Enum } from '../../shared/models/enum.model';

export class Consignataria  {

  public pk: number;
  public cnpj: string;
  public nome: string;
  public nomeFantasia: string;
  public dtInicioContrato: Date;
  public dtFimContrato: Date;
  public logradouro: string;
  public complemento: string;
  public bairro: string;
  public localidade; string;
  public cep:string;
  public estado: any;
  public nomeRepresentante: string;
  public emailRepresentante: string;
  public telefoneRepresentante: string;
  public email: string;
  public telefone: string
  public presidente: string;
  public site: string;
  public emailOuvidoria: string;
  public siteOuvidoria: string;
  public telefoneOuvidoria: string;
  public ddgOuvidoria: string;
  public situacao: Enum;
  public versao: number;
  public numeroBanco: string;
  public nomeBanco: string;
  public agencia: string;
  public conta: string;
  public favorecido: string;
  public banco: any;
}
