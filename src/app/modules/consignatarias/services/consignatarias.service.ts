import { Injectable } from '@angular/core';
import { Consignataria } from '../models/consignataria.model';
import { HttpService } from '../../shared/services/http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConsignatariasService {
  private path: string = "consignatarias";

  constructor(private http: HttpService<Consignataria, number>) {
  }

  get(): Observable<Consignataria>  {
    return this.http.getResource(this.path + '/get');
  }

  findOne(id: number): Observable<Consignataria> {
    return this.http.getOne(this.path, id);
  }

  findAll(): Observable<Consignataria[]> {
    return this.http.get(this.path);
  }

  findByParams(params?: any): Observable<Consignataria[]> {
    return this.http.getParams(this.path + '/search', params);
  }

  saveOrUpdate(model: Consignataria, id: number) : Observable<Consignataria>{
    console.log(id);
     if (id){
       return this.update(id, model);
     }
     return this.save(model);
  }

  save(model: Consignataria): Observable<Consignataria> {
      console.log(model);


    return this.http.post(this.path, model);
  }

  update(id: number, model: Consignataria): Observable<Consignataria> {
    return this.http.patch(this.path, id, model);
  }

  bloquear(id: number, model: {}):Observable<any> {
    return this.http.postResource(id, `${this.path}/bloquear`, model);
  }

  desbloquear(id: number, model: {}):Observable<any> {
    return this.http.postResource(id, `${this.path}/desbloquear`, model);
  }

  listarOperadores(id: number):Observable<any>  {
    let url = `${this.path}/operadores/${id}`;
    return this.http.getResource(url);
  }

}
