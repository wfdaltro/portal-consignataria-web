import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadastraAtualizaConsignatariasComponent } from './pages/cadastra-atualiza-consignatarias/cadastra-atualiza-consignatarias.component';
import { DetalhaConsignatariaComponent } from './pages/detalha-consignataria/detalha-consignataria.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxMaskModule } from 'ngx-mask';


const routes: Routes = [
  {path: 'atualiza/:id', component: CadastraAtualizaConsignatariasComponent,   pathMatch: 'full'},
  {path: '', component: DetalhaConsignatariaComponent,   pathMatch: 'full'},
];

@NgModule({
  declarations: [CadastraAtualizaConsignatariasComponent,  DetalhaConsignatariaComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgxMaskModule.forRoot(),
    DataTablesModule,
    NgbModule,
    NgSelectModule,
  ]
})


export class ConsignatariasModule { }
