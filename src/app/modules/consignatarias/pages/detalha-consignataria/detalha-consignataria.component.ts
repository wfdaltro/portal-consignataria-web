import { Component, OnInit } from '@angular/core';
import { ConsignatariasService } from '../../services/consignatarias.service';
import { Consignataria } from '../../models/consignataria.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detalha-consignataria',
  templateUrl: './detalha-consignataria.component.html',
  styleUrls: ['./detalha-consignataria.component.scss']
})
export class DetalhaConsignatariaComponent implements OnInit {

  consignataria: Consignataria;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private service: ConsignatariasService) { }

  ngOnInit() {
    this.service.get().subscribe((res)=>{
      this.consignataria = res ;
    });
    this.consignataria = new Consignataria();
  }

  public atualizarDados(consignataria: Consignataria){
    this.router.navigate([`/pages/consignataria/atualiza/${consignataria.pk}`]);
  }

}
