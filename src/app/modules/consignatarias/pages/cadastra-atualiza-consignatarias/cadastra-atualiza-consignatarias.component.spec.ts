import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastraAtualizaConsignatariasComponent } from './cadastra-atualiza-consignatarias.component';

describe('CadastraAtualizaConsignatariasComponent', () => {
  let component: CadastraAtualizaConsignatariasComponent;
  let fixture: ComponentFixture<CadastraAtualizaConsignatariasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastraAtualizaConsignatariasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastraAtualizaConsignatariasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
