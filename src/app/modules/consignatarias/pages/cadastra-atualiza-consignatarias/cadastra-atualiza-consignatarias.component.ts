import { Component, OnInit } from '@angular/core';
import { Consignataria } from '../../models/consignataria.model';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Enum } from 'src/app/modules/shared/models/enum.model';
import { ConsignatariasService } from '../../services/consignatarias.service';
import { ToastService } from 'src/app/modules/shared/services/toast.service';
import { Router, ActivatedRoute } from '@angular/router';
import { EnumsService } from '../../../shared/services/enums.service';
import { validarCpf } from 'src/app/modules/shared/validadores/validadores';
import { ValidateBrService } from 'angular-validate-br';
import { BANCOS } from '../../../shared/constantes/bancos';
import { NgxUiLoaderService } from 'ngx-ui-loader'; 

@Component({
  selector: 'app-cadastra-atualiza-consignatarias',
  templateUrl: './cadastra-atualiza-consignatarias.component.html',
  styleUrls: ['./cadastra-atualiza-consignatarias.component.scss']
})
export class CadastraAtualizaConsignatariasComponent implements OnInit {

  public operacao: string;
  consignataria: Consignataria;
  formCadastro: FormGroup;
  submitted: boolean;
  id: any;
  estados: Enum[];
  bancos : any[];

  constructor(private formBuilder: FormBuilder, private service: ConsignatariasService ,private toastService: ToastService,
    private router: Router, private route: ActivatedRoute, private enumsService : EnumsService, private validateBrService: ValidateBrService,
    private ngxService: NgxUiLoaderService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.operacao = "Cadastrar";
    if (this.id){
      this.operacao = "Atuallizar";
    }
    this.consignataria = new Consignataria();
    this.formCadastro  = this.createForm(this.consignataria);
    this.estados = this.enumsService.estado;
    this.bancos = BANCOS;
    if (this.id){
      this.carregarDados();
    }
  }

  carregarDados() {
    this.ngxService.start();
    this.service.findOne(this.id).subscribe((res)=>{
      let entidade: Consignataria  = { ...res };
      entidade.banco = this.getBanco(entidade.numeroBanco);
      console.log(entidade);
      this.formCadastro.patchValue(entidade);
      this.ngxService.stop(); 
    },(err)=>{
      this.ngxService.stop(); 
      this.toastService.showError('Ocorreu um erro ao recuperar o registro. ' + err);
    })
  }
  getBanco(numeroBanco: string): any {
    return BANCOS.filter(b=> b.value === numeroBanco)[0];
  }

  public get form() {
    return this.formCadastro.controls;
  }

  protected createForm(fields: Consignataria): FormGroup {
    return this.formBuilder.group({
      pk: new FormControl(fields.pk ),
      versao: new FormControl(fields.versao ),
      nomeFantasia: new FormControl(fields.nomeFantasia ),
      cnpj: new FormControl(fields.cnpj , [Validators.required, this.validateBrService.cnpj]),
      dtFimContrato: new FormControl(fields.dtFimContrato  ),
      dtInicioContrato: new FormControl(fields.dtInicioContrato  ),
      email: new FormControl(fields.email , [Validators.required]),
      nome: new FormControl(fields.nome , [Validators.required]),
      presidente: new FormControl(fields.presidente  ),
      telefone: new FormControl(fields.telefone  ),
      ddgOuvidoria: new FormControl(fields.ddgOuvidoria ),
      telefoneOuvidoria: new FormControl(fields.telefoneOuvidoria ),
      emailOuvidoria: new FormControl(fields.emailOuvidoria ),
      siteOuvidoria: new FormControl(fields.siteOuvidoria ),
      logradouro: new FormControl(fields.logradouro  ),
      complemento: new FormControl(fields.complemento  ),
      bairro: new FormControl(fields.bairro  ),
      cep: new FormControl(fields.cep  ),
      localidade: new FormControl(fields.localidade  ),
      estado: new FormControl(fields.estado ),
      site: new FormControl(fields.site),
      nomeRepresentante: new FormControl(fields.nomeRepresentante ),
      emailRepresentante: new FormControl(fields.emailRepresentante  ),
      telefoneRepresentante: new FormControl(fields.telefoneRepresentante  ),
      banco: new FormControl(fields.banco  ),
      agencia: new FormControl(fields.agencia  ),
      conta: new FormControl(fields.conta  ),
      favorecido: new FormControl(fields.favorecido  ),

    });
  }


  public saveOrUpdate(){
    this.formCadastro.markAllAsTouched();
    this.submitted = true;
    if (this.formCadastro.invalid) {
      window.scroll(0,0);
      return;
    }
    let entidade: Consignataria  = { ...this.formCadastro.value };
    entidade.numeroBanco = entidade.banco?.value || '';
    entidade.nomeBanco = entidade.banco?.label || '';
    console.log('entidade1 :' , entidade);
    this.ngxService.start(); 
    this.service.saveOrUpdate(entidade, this.id).subscribe((res) => {
      console.log(res);
      this.ngxService.stop(); 
      this.router.navigate(['pages/consignataria']);
      this.toastService.showSuccess('Registro salvo com sucesso!');

    }, (err) =>{
      this.ngxService.stop(); 
      this.toastService.showError('Ocorreu um erro ao salvar o registro. ' + err);
    });
  }

}
