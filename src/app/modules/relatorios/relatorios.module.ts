import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RelatoriosComponent } from './pages/relatorios/relatorios.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { NgxMaskModule } from 'ngx-mask';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';


const routes: Routes = [
  {path: '', component: RelatoriosComponent,   pathMatch: 'full'},
];

@NgModule({
  declarations: [RelatoriosComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule,
    NgxMaskModule.forRoot(),
    NgbModule,
    NgSelectModule,
  ]
})
export class RelatoriosModule { }
