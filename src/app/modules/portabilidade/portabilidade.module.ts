import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultaEmprestimosPortabilidadeComponent } from './pages/consulta-emprestimos-portabilidade/consulta-emprestimos-portabilidade.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { NgxMaskModule } from 'ngx-mask';
import { DataTablesModule } from 'angular-datatables';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { CadastroPortabilidadeComponent } from './pages/cadastro-portabilidade/cadastro-portabilidade.component';
import { NgxCurrencyModule } from 'ngx-currency';
import { DetalhaPortabilidadeComponent } from './pages/detalha-portabilidade/detalha-portabilidade.component';
import { AverbacaoPortabilidadeComponent } from './pages/averbacao-portabilidade/averbacao-portabilidade.component';
import { IndeferimentoPortabilidadeComponent } from './pages/indeferimento-portabilidade/indeferimento-portabilidade.component';
 

const routes: Routes = [
  {path: '', component: ConsultaEmprestimosPortabilidadeComponent,   pathMatch: 'full'},
  {path: 'novo', component: CadastroPortabilidadeComponent,   pathMatch: 'full'},
  {path: 'averbar/:id', component: AverbacaoPortabilidadeComponent,   pathMatch: 'full'},
  {path: 'indeferir/:id', component: IndeferimentoPortabilidadeComponent,   pathMatch: 'full'},
  {path: ':id', component: DetalhaPortabilidadeComponent,   pathMatch: 'full'},
];

@NgModule({
  declarations: [ConsultaEmprestimosPortabilidadeComponent, CadastroPortabilidadeComponent,
     DetalhaPortabilidadeComponent, AverbacaoPortabilidadeComponent, IndeferimentoPortabilidadeComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgxMaskModule.forRoot(),
    DataTablesModule,
    NgbModule,
    NgSelectModule,
    NgxDropzoneModule,
    NgxCurrencyModule
  ]
})
export class PortabilidadeModule { 
   
}
