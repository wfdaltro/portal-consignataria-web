import { EmprestimoConsignado } from '../../consignacoes/models/emprestimo-consignado.model';
import { Consignataria } from '../../consignatarias/models/consignataria.model';
import { Enum } from '../../shared/models/enum.model';

export class Portabilidade{
 
    pk: number;
    
	 emprestimo: EmprestimoConsignado;
 
	 valorPrestacao: number;

 
 totalDeParcelas: number;
 
	 jurosAoMes: number;

	 
 cet: number;
	
	 
	 consignataria: Consignataria

	
 
 situacao: Enum;
  }
  