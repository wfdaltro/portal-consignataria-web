import { Injectable } from '@angular/core';
import { HttpService } from '../../shared/services/http.service';
import { Portabilidade } from '../models/portabilidade.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PortabilidadeService {

  private path: string = "portabilidade";
 
  constructor(private http: HttpService<any, number>) { }

  findEmprestimoByServidor(pk: number) {
    return this.http.getResource(`emprestimosConsignados/servidor/${pk}`);
  }

   salvarPortabilidade(portabilidade: Portabilidade) : Observable<any> {
    return this.http.post(this.path ,portabilidade);
  }

  findByParams(params?: any): Observable<Portabilidade[]> {
    return this.http.getParams(this.path + '/search', params);
  }

  findOne(id: number): Observable<Portabilidade> {
    return this.http.getOne(this.path, id);
  }

  finalizar(pk: number, token: string): Observable<any> {
    return this.http.pathResource(pk, `${this.path}/finalizar`, token);
  }
 


}
