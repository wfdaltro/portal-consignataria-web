import { Component, OnInit } from '@angular/core';
import { Portabilidade } from '../../models/portabilidade.model';
import { PortabilidadeService } from '../../services/portabilidade.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { EmprestimoConsignado } from 'src/app/modules/consignacoes/models/emprestimo-consignado.model';

@Component({
  selector: 'app-detalha-portabilidade',
  templateUrl: './detalha-portabilidade.component.html',
  styleUrls: ['./detalha-portabilidade.component.scss']
})
export class DetalhaPortabilidadeComponent implements OnInit {

  portabilidade: Portabilidade;
  emprestimo: EmprestimoConsignado;

  constructor(private portabilidadeService: PortabilidadeService, private toastr: ToastrService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.portabilidadeService.findOne(+id).subscribe((res)=>{
      this.portabilidade = res;
      this.emprestimo = this.portabilidade.emprestimo;
    });
  }

  indeferir(){
    this.router.navigate([`/pages/portabilidade/indeferir/${this.portabilidade.pk}`]);
  }

  averbar(){
      this.router.navigate([`/pages/portabilidade/averbar/${this.portabilidade.pk}`]);
  }

}
