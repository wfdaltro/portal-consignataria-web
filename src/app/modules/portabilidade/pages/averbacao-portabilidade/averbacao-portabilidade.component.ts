import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { EmprestimoConsignado } from 'src/app/modules/consignacoes/models/emprestimo-consignado.model';
import { Portabilidade } from '../../models/portabilidade.model';
import { PortabilidadeService } from '../../services/portabilidade.service';

@Component({
  selector: 'app-averbacao-portabilidade',
  templateUrl: './averbacao-portabilidade.component.html',
  styleUrls: ['./averbacao-portabilidade.component.scss']
})
export class AverbacaoPortabilidadeComponent implements OnInit {

  portabilidade: Portabilidade;
  emprestimo: EmprestimoConsignado;
  tokenAverbacao: string;

  constructor(private portabilidadeService: PortabilidadeService, private toastr: ToastrService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.portabilidadeService.findOne(+id).subscribe((res)=>{
      console.log(res);
      this.portabilidade = res;
      this.emprestimo = this.portabilidade.emprestimo;
    });
  }

  aceitar(){
    if (!this.tokenAverbacao){
      this.toastr.error('Informe o token de aceite da portabilidade.','Erro ao processar a requisição', {
        timeOut: 3000
      });
      return;
    }

    this.portabilidadeService.finalizar(this.portabilidade.pk, this.tokenAverbacao ).subscribe((res)=>{
      this.toastr.success('Portabilidade averbada com sucesso!','Sucesso!', {
        timeOut: 3000
      });
      this.router.navigate([`/pages/portabilidade`]);
    },(err)=>{
      this.toastr.error(err,'Erro ao processar a requisição', {
        timeOut: 3000
      });
    });
  }
}
