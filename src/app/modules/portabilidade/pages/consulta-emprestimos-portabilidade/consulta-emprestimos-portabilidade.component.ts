import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { DATATABLE_PT } from 'src/app/modules/shared/constantes/datatable';
import { Router } from '@angular/router';
import { Enum } from 'src/app/modules/shared/models/enum.model';
import { Consignataria } from 'src/app/modules/consignatarias/models/consignataria.model';
import { Servidor } from 'src/app/modules/servidores/models/servidor.model';
import { EnumsService } from 'src/app/modules/shared/services/enums.service';
import { ServidoresService } from 'src/app/modules/servidores/services/servidores.service';
import { ConsignatariasService } from 'src/app/modules/consignatarias/services/consignatarias.service';
import { Portabilidade } from '../../models/portabilidade.model';
import { PortabilidadeService } from '../../services/portabilidade.service';

@Component({
  selector: 'app-consulta-emprestimos-portabilidade',
  templateUrl: './consulta-emprestimos-portabilidade.component.html',
  styleUrls: ['./consulta-emprestimos-portabilidade.component.scss']
})
export class ConsultaEmprestimosPortabilidadeComponent implements OnInit {

  filtroPesquisa: any = {};
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  situacoes: Enum[]
  consignatarias: Consignataria[];
  servidores: Servidor[];
  tiposRegistro: Enum[];
  portabilidades: Portabilidade[];


  constructor(private router: Router, private enumsService: EnumsService, private portabilidadeService: PortabilidadeService,
    private servidoresService: ServidoresService, private consignatariasService: ConsignatariasService) { 

  }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language: DATATABLE_PT
    };
    this.carregarDados();
    this.carregarServidores();
    this.carregarSituacoes();
    this.carregarTiposRegistro();
   this.carregarConsignatarias();
  }

  carregarConsignatarias() {
    this.consignatariasService.findAll().subscribe((res)=>{
      this.consignatarias = res
    });
   }

  carregarServidores() {
    this.servidoresService.findByParams().subscribe((res)=>{
      this.servidores = res
    });
   }
 
   carregarSituacoes() {
     this.situacoes =  this.enumsService.situacaoPortabilidade;
   }

   carregarTiposRegistro() {
    this.tiposRegistro =  this.enumsService.tipoRegistroPortabilidade;
  }

   carregarDados() {
    this.pesquisar();
    }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  public pesquisar(): void {
    this.portabilidadeService.findByParams(this.filtroPesquisa).subscribe((res)=>{
      this.portabilidades = res;
      this.dtTrigger.next();
    },(error)=>{
      console.log(error);
    });
  }

  public detalhar(portabilidade: Portabilidade){
    console.log(portabilidade);
    this.router.navigate([`/pages/portabilidade/${portabilidade.pk}`]);
  }


}
