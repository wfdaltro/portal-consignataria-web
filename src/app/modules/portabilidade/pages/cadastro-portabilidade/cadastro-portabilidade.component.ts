import { Component, OnInit } from '@angular/core';
import { ServidoresService } from '../../../servidores/services/servidores.service';
import { Servidor } from 'src/app/modules/servidores/models/servidor.model';
import { ToastrService } from 'ngx-toastr';
import { EmprestimoConsignado } from 'src/app/modules/consignacoes/models/emprestimo-consignado.model';
import { Subject } from 'rxjs';
import { DATATABLE_PT } from 'src/app/modules/shared/constantes/datatable';
import { PortabilidadeModule } from '../../portabilidade.module';
import { PortabilidadeService } from '../../services/portabilidade.service';
import { Portabilidade } from '../../models/portabilidade.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cadastro-portabilidade',
  templateUrl: './cadastro-portabilidade.component.html',
  styleUrls: ['./cadastro-portabilidade.component.scss']
})
export class CadastroPortabilidadeComponent implements OnInit {


  chave: string;
  servidor: Servidor;
  fase : number = 1

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  emprestimos: EmprestimoConsignado[];
  emprestimo: EmprestimoConsignado;
  portabilidade: Portabilidade

  constructor(private router: Router, private servidoresService: ServidoresService, 
    private toastr: ToastrService, private portabilidadeService: PortabilidadeService) { }

  ngOnInit(): void {
  }

  pesquisarServidor(){
    if (!this.chave){
      this.toastr.error('Informe o CPF ou a matrícula do Servidor.','Erro ao processar a requisição', {
        timeOut: 3000
      });
      return;
    }
    this.servidoresService.findByCpfMatricula(this.chave).subscribe((res)=>{
      this.servidor = res;
    });
  }

  iniciarProcesso(){
    this.fase = 2;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language: DATATABLE_PT
    };
    this.pesquisar();
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  public pesquisar(): void {
    this.portabilidadeService.findEmprestimoByServidor(this.servidor.pk).subscribe((res)=>{
      this.emprestimos = res;
    },(error)=>{
      console.log(error);
    });
  }

  selecionar(emprestimo: EmprestimoConsignado){
    this.fase = 3;
    this.emprestimo = emprestimo;
    this.portabilidade = new Portabilidade();
  }

  salvar(){
    if (!this.portabilidade.cet || !this.portabilidade.jurosAoMes || !this.portabilidade.totalDeParcelas || !this.portabilidade.valorPrestacao){
      this.toastr.error('Informe todos os dados da portabilidade.','Erro ao processar a requisição', {
        timeOut: 3000
      });
      return;
    }
    this.portabilidade.emprestimo = {
      pk: this.emprestimo.pk
    } as EmprestimoConsignado;
    this.portabilidadeService.salvarPortabilidade(this.portabilidade).subscribe((res)=>{
      this.toastr.success('Portabilidade gerada com sucesso!','Sucesso!', {
        timeOut: 3000
      });
      this.router.navigate([`/pages/portabilidade`]);
    },(err)=>{
      this.toastr.error(err,'Erro ao processar a requisição', {
        timeOut: 3000
      });
    });
  }

}
