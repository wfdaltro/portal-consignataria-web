import { Enum } from '../../shared/models/enum.model';

export class Servidor {
  pk: number;
  matricula: string;
  nome: string;
  cpf: string;
  rg: string;
  lotacao: string;
  orgaoExpedidorRg: string;
  ufRg: string;
  dataExpedicaoRg: string;
  dataAdmissao: string;
  nacionalidade: string;
  naturalidade: string;
  cargo: string;
  funcao: string;
  filiacaoMae: string;
  filiacaoPai: string;
  sexo:any;
  estadoCivil:any;
  dataNascimento: string;
  telefone: string;
  email: string;
  celular: string;
  ramal: string;
  rendaMensal: number;
  numeroBanco: string;
  nomeBanco: string;
  agencia: string;
  conta: string;
  margemEmprestimo: number;
  margemCartao: number;
  margemEmprestimoDisponivel: number;
  margemCartaoDisponivel: number;
  margemCartaoReservada: number;
  margemEmprestimoReservada: number;
  logradouro: string;
  complemento: string;
  bairro: string;
  localidade; string;
  cep:string;
  estado: any;
  usuarioSistema: boolean;
  senhaExpirada: boolean;
  gestor: boolean;
  situacao: Enum;

  descricao: string = this.matricula;

}
