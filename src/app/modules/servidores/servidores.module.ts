import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultaServidoresComponent } from './pages/consulta-servidores/consulta-servidores.component';
import { DetalhaServidoresComponent } from './pages/detalha-servidores/detalha-servidores.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { NgxMaskModule } from 'ngx-mask';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';



const routes: Routes = [
  {path: '', component: ConsultaServidoresComponent,   pathMatch: 'full'},
  {path: ':id', component: DetalhaServidoresComponent,   pathMatch: 'full'}
];

@NgModule({
  declarations: [ConsultaServidoresComponent, DetalhaServidoresComponent,],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    DataTablesModule,
    NgxMaskModule.forRoot(),
    NgbModule,
    NgSelectModule,
  ]
})


export class ServidoresModule { }
