import { Injectable } from '@angular/core';
import { HttpService } from '../../shared/services/http.service';
import { Servidor } from '../models/servidor.model';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServidoresService {

  private path: string = "servidores";

  constructor(private http: HttpService<Servidor, number>) {
  }

  findOne(id: number): Observable<Servidor> {
    return this.http.getOne(this.path, id);
  }

  findAll(): Observable<Servidor[]> {
    return this.http.get(this.path);
  }


  findByParams(params?: any): Observable<Servidor[]> {
    return this.http.getParams(this.path + '/search', params);
  }

  findByCpfMatricula(chave: string): Observable<Servidor> {
   return this.http.getResource(this.path+ '/cpfMatricula?k='+ chave);
  }

}
