import { Component, OnInit } from '@angular/core';
import { Servidor } from '../../models/servidor.model';
import { Router, ActivatedRoute } from '@angular/router';
import { ServidoresService } from '../../services/servidores.service';

@Component({
  selector: 'app-detalha-servidores',
  templateUrl: './detalha-servidores.component.html',
  styleUrls: ['./detalha-servidores.component.scss']
})
export class DetalhaServidoresComponent implements OnInit {

  servidor: Servidor;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private service: ServidoresService) { }

  ngOnInit() {
    let idServidor = this.route.snapshot.paramMap.get('id');
    this.service.findOne(+idServidor).subscribe((res)=>{
      this.servidor = res;
    });
  }

  ocorrencia(servidor: Servidor){
    if (servidor.situacao.codigo === 'A'){
      this.router.navigate([`/pages/servidores/bloqueia/${servidor.pk}`]);
    }else{
      this.router.navigate([`/pages/servidores/desbloqueia/${servidor.pk}`]);
    }
  }
}
