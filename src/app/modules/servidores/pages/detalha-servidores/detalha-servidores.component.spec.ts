import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalhaServidoresComponent } from './detalha-servidores.component';

describe('DetalhaServidoresComponent', () => {
  let component: DetalhaServidoresComponent;
  let fixture: ComponentFixture<DetalhaServidoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalhaServidoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalhaServidoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
