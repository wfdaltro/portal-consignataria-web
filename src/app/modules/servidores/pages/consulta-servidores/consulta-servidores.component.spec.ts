import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaServidoresComponent } from './consulta-servidores.component';

describe('ConsultaServidoresComponent', () => {
  let component: ConsultaServidoresComponent;
  let fixture: ComponentFixture<ConsultaServidoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultaServidoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaServidoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
