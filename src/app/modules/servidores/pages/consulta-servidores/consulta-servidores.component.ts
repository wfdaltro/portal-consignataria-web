import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { DATATABLE_PT } from 'src/app/modules/shared/constantes/datatable';
import { Servidor } from '../../models/servidor.model';
import { ServidoresService } from '../../services/servidores.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-consulta-servidores',
  templateUrl: './consulta-servidores.component.html',
  styleUrls: ['./consulta-servidores.component.scss']
})
export class ConsultaServidoresComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  servidores: Servidor[];
  filtroPesquisa: any = {};

  constructor(private router: Router, private service: ServidoresService) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language: DATATABLE_PT
    };
    this.carregarDados();
  }

 carregarDados() {
    this.service.findAll().subscribe((res)=>{
      this.servidores = res;
      this.dtTrigger.next();
    },(error)=>{
      console.log(error);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  public pesquisar(): void {
    this.service.findByParams(this.filtroPesquisa).subscribe((res)=>{
      this.servidores = res;
    },(error)=>{
      console.log(error);
    });
  }

  public detalhar(servidor: Servidor){
    this.router.navigate([`/pages/servidores/${servidor.pk}`]);
  }


}
