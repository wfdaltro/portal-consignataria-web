import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { SessionService } from './modules/shared/services/session.service';

@Injectable({
    providedIn: 'root'
})
export class AppGuard implements CanActivate {

    constructor(private router: Router, private session: SessionService  ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const token = this.session.getSessionToken();
        if (token) {
            return true;
        }
        this.router.navigate(['/auth/login']);
        return false;
    }
}
