import {Injectable} from '@angular/core';

export interface NavigationItem {
  id: string;
  title: string;
  type: 'item' | 'collapse' | 'group';
  translate?: string;
  icon?: string;
  hidden?: boolean;
  url?: string;
  classes?: string;
  exactMatch?: boolean;
  external?: boolean;
  target?: boolean;
  breadcrumbs?: boolean;
  function?: any;
  badge?: {
    title?: string;
    type?: string;
  };
  children?: Navigation[];
}

export interface Navigation extends NavigationItem {
  children?: NavigationItem[];
}

const NavigationItems = [
  {
    id: 'navigation',
    title: ' ',
    type: 'group',
    icon: 'feather icon-monitor',
    children: [
      {
        id: 'dashboard',
        title: 'Início',
        type: 'item',
        url: '/pages/dashboard',
        icon: 'feather icon-home',
        classes: 'nav-item'
      },
      {
        id: 'config',
        title: 'Configurações',
        type: 'collapse',
        icon: 'feather icon-settings',
        children: [
          {
            id: 'dadosEA',
            title: 'Dados Consignatária',
            type: 'item',
            url: '/pages/consignataria',
          }
        ]
      },
      {
        id: 'cadastros',
        title: 'Cadastros',
        type: 'collapse',
        icon: 'feather icon-layout',
        children: [
          {
            id: 'usuarios',
            title: 'Usuários',
            type: 'item',
            url: '/pages/usuarios',
          },
          {
            id: 'produtos',
            title: 'Produtos',
            type: 'item',
            url: '/pages/produtos',
          },
        ]
      },
      {
        id: 'emprestimos',
        title: 'Empréstimos',
        type: 'collapse',
        icon: 'feather icon-hash',
        children: [
          {
            id: 'proposta_emprestimo',
            title: 'Proposta',
            type: 'item',
            url: '/pages/propostas/emprestimo',
          },
          {
            id: 'analise_emprestimo',
            title: 'Analise',
            type: 'item',
            url: '/pages/propostas/emprestimo/consulta',
          },
          {
            id: 'contratos',
            title: 'Contratos',
            type: 'item',
            url: '/pages/emprestimos',
          },
        ]
      },
      {
        id: 'cartoes',
        title: 'Cartões',
        type: 'collapse',
        icon: 'feather icon-credit-card',
        children: [
          {
            id: 'proposta_cartão',
            title: 'Proposta',
            type: 'item',
            url: '/pages/propostas/cartao',
          },
          {
            id: 'analise_cartao',
            title: 'Análise',
            type: 'item',
            url: '/pages/propostas/cartao/consulta',
          },
          {
            id: 'contratos_cartoes',
            title: 'Contratos',
            type: 'item',
            url: '/pages/cartoes',
          },
        ]
      },
      {
        id: 'portabilidade',
        title: 'Portabilidade',
        type: 'collapse',
        icon: 'feather icon-credit-card',
        children: [
          {
            id: 'proposta_portabilidade',
            title: 'Proposta',
            type: 'item',
            url: '/pages/portabilidade/novo',
          },
          {
            id: 'analise_portabilidade',
            title: 'Análise',
            type: 'item',
            url: '/pages/portabilidade',
          },
        ]
      },
       
      {
        id: 'renegociacao',
        title: 'Renegociação',
        type: 'item',
        url: '/pages/renegociacao',
        icon: 'feather icon-repeat',
        classes: 'nav-item'
      },
      
    ]
  },

];

@Injectable()
export class NavigationItem {
  public get() {
    return NavigationItems;
  }
}
